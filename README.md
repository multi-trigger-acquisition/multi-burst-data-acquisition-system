Authors: Pawel Kozlowski, Alexandre Lasheen

Affiliation: Warsaw Univ. Of Technology, CERN

To make it work:
1. Download NI-VISA-17.0.0 for linux
2. ./INSTALL
3. It might happen that an error will be prompted after restart
google: how to install DAQUmx 8.0.2 and LabView 2011 CentOS 6

yum install gcc gcc-c++ kernel-devel kernel-headers make
4. Install NIKAL151.ISO
5. When strugging with "do_munmap not enough arguments" error,
modify the file : /var/lib/nikal/3.10.0-693.2.2.el7.x86_64/nikal/nikal.c
change the line 3723:
return do_munmap(mm,addr,len);
with the following line:
return do_munmap(mm,addr,len,NULL);
save and rerun updateNIDrivers
 
6. Add memory_profiler.py to the folder MBTrgr/libraries. More https://pypi.org/project/memory-profiler/
7. Add inspectorpy.py to the folder /TriggerInspector. This is a python bridge to the Inspector. More info: https://wikis.cern.ch/display/INSP/Python+Integration