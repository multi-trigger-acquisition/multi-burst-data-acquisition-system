'''
Created on 21 mars 2018

@author: P. Kozlowski and A.Lasheen

Some tests to get data from Tektronix oscilloscope through pyvisa
'''

#!a 50ms delay must occur between GPIB commands

import visa
import matplotlib.pyplot as plt
import pylab
import numpy as np

rm = visa.ResourceManager()

# Select IP address of the scope
ip_address = '128.141.56.41'
gpip_address = 'gpib0,1'



#BEAM WAVE
#name
beamName = 'BEAM'
# Select data source
beamChannel = 2
# select termination
beamTermination = '1E+6'
#beam v REF position
beamVRPosition = 75
#beam v position
beamVPosition = 1
#coupling
beamCouplingLst = ['AC','DC','GND']
beamCoupling = beamCouplingLst[0]
#vertical scale [e.g. 100mv per division]
beamVScale = '100E-03'

#TRIGGER
#trigger name
triggerName = 'TRIGGER'
#select trigger source (CH1)
triggerSrcLst = ['AUXiliary', 'CH1', 'CH2', 'CH3', 'CH4', 'LINE']
triggerSrc = triggerSrcLst[1]
triggerCHInt=1
print('trigger triggerSrc "%s"' %(triggerSrc))
#select trigger slope
triggerSlopeLst = ['RISe','FALL']
triggerSlope = triggerSlopeLst[0]
#select trigger coupling 
triggerCouplingLst = ['DC','AC','HFRej']
triggerCoupling = triggerCouplingLst[0]
#select triggel level (<NR3> - fp with exponent)
triggerLevelLst = ['ECL','TTL']
triggerLevel = triggerLevelLst[1]
#Trigger mode
triggerModeList = ['AUTO','NORMal']
triggerMode = triggerModeList[1]
#Trigger horizontal position
#0-100 (integer)
triggerHPosition = 0
#Trigger termination
triggerTermination = '1E+6'
#Trigger vertical REF position
triggerVRPosition = 25
#trigger vertical position
triggerVPosition = -1
#vertical scale
triggerVScale = '1E+00'


#Setting connectivity
scope_tpcip = 'TCPIP0::%s::%s::INSTR' % (ip_address, gpip_address)
#Loading the scope
scope = rm.open_resource(scope_tpcip)
#Checking if session is correctly established
print(scope.query('*IDN?'))

#timeout
scope.timeout = 4000

# Set return to factory settings
#scope.write('FACtory')

#turn off the acquisition system
scope.write('acquire:state 0')

#set acquire mode to sample
scope.write('acquire:mode sample')

#select channel 2 
scope.write('SELECT:CH2 ON')


#TRIGGER SETUP
#define name
scope.write('CH%d:LABEL:NAMe %s' %(triggerCHInt, triggerName))
#Set channel of the trigger {AUXiliary | CH<x> | LINE}
scope.write('TRIGger:A:EDGE:SOUrce %s' % triggerSrc)
#scope.write('TRIGger:A:LOGIC:INPut:%s' % triggerSrc)
#Set edge of the trigger
#Slope
scope.write('TRIGger:A:EDGE:SLOpe %s' % triggerSlope)
#coupling
scope.write('TRIGger:A:EDGE:COUPling %s' % triggerCoupling)
#level
scope.write('TRIGger:A:LEVel %s' % triggerLevel)
#mode (normal)
scope.write('TRIGger:A:MODe %s' % triggerMode)
#h-position
scope.write('HORizontal:TRIGger:POSition %d' % triggerHPosition)
#termination
scope.write('CH%d:TERmination %s' %(triggerCHInt, triggerTermination))
#vertical REF position
#scope.write('REF%d:VERTical:POSition %d' %triggerCHInt, triggerVRPosition)
#vertical position <-8;8>
scope.write('CH%d:POSition %d' %(triggerCHInt, triggerVPosition))
#Vertical Scale
scope.write('CH%d:SCAle %s' %(triggerCHInt, triggerVScale))

#BEAM SIGNAL SETUP
#setting the name
scope.write('CH%d:LABEL:NAMe %s' %(beamChannel, beamName))
#Vertical REF position
#scope.write('REF%d:VERTical:POSition %d' %beamChannel, beamVRPosition)
#Vertical REF Scale
#scope.write('REF%d:VERTical:SCAle%d' %beamChannel, beamVRScale)
#Termination
scope.write('CH%d:TERmination %s' % (beamChannel, beamTermination))
#Set coupling in (AC mode)
scope.write('CH%d:COUPling %s' % (beamChannel, beamCoupling))
#vertical position <-8;8>
scope.write('CH%d:POSition %d' % (beamChannel, beamVPosition))
#vertical scale 
scope.write('CH%d:SCAle %s' % (beamChannel, beamVScale))



#GENERAL VIEW
##Main time base
#Horizontal scale (2 us per divison)
horizontalMainScale = '2E-7'
#horizontal main position
horizontalMainPos = '5E+1'#50
#sample rate, attention! has influence on record length
mainSampleRate = '125E5'
##position on the display
#horizontal scale is equivalent to horizontal main scale
#horizontalScale
#horizontal resolution e.g. 5000 = 5000 data points will be acquired each record
horizontalResolution = 500
#horizontal position -10% ov waveform is to the left of the screen center
horizontalPosition = 10
#horizontal recordlength -sets record length to the number of data points in each frame 
#atention! has influence on sample rate
horizontalRecordLen =  500
##FastFrame - memory segmentation, captures a series of triggered acquisitions with minimal intervening time.
#reference frame - 2nd frame
fastFrameReferenceFrame = 1
#frame count - number of frames to acquire
fastFrameFrameCount = 2
#frame length
fastFrameFrameLength = 500


#MAIN independent on delay
#HORIZONTAL
##horizontal main scale
scope.write('HORizontal:MAIn:SCAle %s' % horizontalMainScale)
#horizontal main position <NR3> 0 - 100%
scope.write('HORizontal:MAIn:POSition %s' % horizontalMainPos)
#sample rate - [no of samples per second] - the record length is automatically adjusted to maintain a constant number of
#data points in each frame e.g. 125 MS/s : 125E5
scope.write('HORizontal.MAIn:SAMPLERate %s' % mainSampleRate)

##position on the display
#horizontal resolution - seths horizontal record length to the number of data points in each frame. The sample rate is 
#automatically adjusted at the same time to maintain a constant time per division
scope.write('HORizontal:RESOlution %d' % horizontalResolution)
#horizontal position - equivalent with the 'main' HOR:POS if the delay mode is turned off
scope.write('HORizontal:POSition %d' % horizontalPosition)
#horizontal recordlength
scope.write('HORizontal:RECOrdlength %d' % horizontalRecordLen)



##FastFrame
#reference frame no is used to calculate time differences for each frame and displays those differences in the graticule
scope.write('HORizontal:FASTfram:REF:FRAME %d' % fastFrameReferenceFrame)
#sets or returns fastframe frame count
scope.write('HORizontal:FASTframe:COUNt %d' % fastFrameFrameCount)
#horizontal record length to the number of sample points per frame
scope.write('HORizontal:FASTframe:LENgth %d' % fastFrameFrameLength)
#turn on fast frame
scope.write('horizontal:fastframe:state 1')   

#VERTICAL
#write beam waveform data
# scope.write('DATa:SOURce CH%d' % (beamChannel))
scope.write('DAT:SOUR CH%d' % (beamChannel))
#write trigger data
#scope.write('DAT:SOUR CH%d' % (triggerCHInt))

scope.write('ACQUIRE:STOPAFTER SEQUENCE')

#turn on the acquisition system
scope.write('acquire:state 1')
#scope.write('ACQUIRE:STATE ON')

#wait for the data to be acquired
scope.write('*WAI')

# # Get raw data
# data = scope.query('CURV?')
# data = scope.query('WAVFRM?')

#Controls the settings concerning the X-axis.
#INPut:POSition:X
#scope.write('INPut:POSition:%d' % (xposition))

# Get the data (converted directly from binary)
#data = scope.query_binary_values('CURV?', datatype='i')
data = scope.query_binary_values('CURV?', datatype='h')

plt.figure('Data')
plt.clf()
plt.plot(data)
pylab.show()


#scope.write('horizontal:fastframe:count {0}'.format(10))
#scope.write('ch2:scale {0}'.format(0.002))

# get current trigger paramteters fort the insturment
#data2 = scope.query_ascii_values('TRIGger?')
#values = np.array_str(scope.query_ascii_values('TRIGger:A:EDGE:SOUrce?'))

#data = scope.query_binary_values('horizontal:fastframe:count?', datatype='i')
# Figure









