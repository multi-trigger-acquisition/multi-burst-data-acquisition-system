from inspectorpy import InspectorCom
import json


print('inspectorpy imported')
inspector = InspectorCom()
print('inspector initialized ')

#write mode = 0
#load mode = 1

name = inspector.getInputVariable('$name')
print(name)
mode = inspector.getInputVariable('$mode') 

if (mode == 0):
    class Panel:
        def toJSON(self):
            return json.dumps(self, default=lambda o: o.__dict__, 
                              sort_keys=True, indent=4)


if (mode == 1):
    class Panel:
        def __init__(self, j):
            self.__dict__ = json.loads(j)



#write
if (mode==0) :
    panel = Panel()
    panel.cycleName = inspector.getInputVariable('$cycleName') 
    panel.timeout = inspector.getInputVariable('$timeout')
    print(panel.timeout)
    panel.horizontalPosition = inspector.getInputVariable('$horizontalPosition') 
    panel.triggerChannel = inspector.getInputVariable('$triggerChannel') 
    panel.beamChannel = inspector.getInputVariable('$beamChannel')
    with open('savedSettings/%s.json' %name, 'w') as outfile:
        print(panel.toJSON(), file=outfile)
    #print(panel.toJSON())


#read
if (mode==1):
    with open('savedSettings/%s.json' %name) as json_file:
        parsed = json.load(json_file)
        stringData = json.dumps(parsed, indent=4, sort_keys=True)
        print(stringData)
        panel = Panel(stringData)
         
    inspector.setOutputVariable('$cycleName', panel.cycleName)
    print(panel.cycleName)
    inspector.setOutputVariable('$timeout', panel.timeout)
    print(panel.timeout)
    inspector.setOutputVariable('$horizontalPosition', panel.horizontalPosition)
    print(panel.horizontalPosition)
    inspector.setOutputVariable('$triggerChannel', panel.triggerChannel)
    print(panel.triggerChannel)
    inspector.setOutputVariable('$beamChannel', panel.beamChannel)
    print(panel.beamChannel)


inspector.end()
