# -*- coding: utf-8 -*-
import visa
#%%settings object
#!a 50ms delay must occur between GPIB commands
class setupObject(object):
    
    '''
    The object is supposed to include the following information (simplified version):
    ip_address = '128.141.96.95'
    gpip_address = 'gpib0,3'
    timeout
    timeout = 400 #[s]
    scope_tpcip = 'TCPIP0::%s::%s::INSTR' % (ip_address, gpip_address)
    rm = visa.ResourceManager()
    scope = rm.open_resource(scope_tpcip)
    '''
 
    def __init__(self):
        '''
                Creation of a setup object
        '''
        self.rm = visa.ResourceManager()

        

    def setDataFormat(self):
        '''
        This specifies a normal waveform where one
        binary data point is transmitted for each point in the
        waveform record. Only Y values are explicitly transmitted.
        Absolute coordinates are given by:
        Xn = XZEro + XINcr (N-PT_Off)
        The function has to be executed after manual initialisation
        (after running initialize(ip_address_i, gpip_address_i, timeout_i)).
        '''
        # str, # Point format (Y/ENV)
        self.scope.write("WFMOutpre:PT_Fmt Y")
        # int, # number of bits per waveform point
        self.scope.write('wfmoutpre:bit_n 8')
        #Set correct format of binary data in the scope (unsigned integer)
        #str, # binary format of waveform
        self.scope.write("WFMOutpre:BN_Fmt RP")
        #Data mode - the basic one
        self.scope.write("ACQuire:MODe SAMple")
        #set encoding to one byte per datapoint
        #int, # data width for waveform
        self.scope.write("DATa:WIDth 1")
        #binary data encoding with positive integer, most significant byte first
        # self.write("DATa:ENCdg ASCIi")
        # or RPBinary for faster data transfer - str, # encoding of waveform (binary/ascii)
        self.scope.write("DATa:ENCdg RPBinary")
    
    
    def rstFactorySettings(self):
        self.scope.write('FACtory')
        print('reset to factory settings succeeded')
    
    
    def turnOffAcqSystem(self):
        #turn off the acquisition system
        self.scope.write('acquire:state 0')
        
        
    def turnOnAcqSystem(self):
        #turn on the acquisition system
        self.scope.write('acquire:state 1')


    def changeTimeout(self, SWtimeout):
        #a software timeout (on a client side)
        self.SWtimeout = SWtimeout
        self.reinitialize(self)


    def changeGpipAddr(self, gpip_address):

        self.gpip_address = gpip_address
        self.reinitialize(self)


    @property
    def changeIPAddr(self, ip_address):
        
        self.ip_address = ip_address
        self.reinitialize(self)


    def reinitialize(self):
        '''
            Todo after each change of any of the three attributes
        '''
        #self.rm = visa.ResourceManager()
        self.scope_tpcip = 'TCPIP0::%s::%s::INSTR' % (self.ip_address, self.gpip_address)
        # Loading the scope
        self.scope = self.rm.open_resource(self.scope_tpcip)
        # Checking if scope is ok
        print(self.scope.query('*IDN?'))


        
        
    def calibrate(self):
        self.scope.write('CALibrate:FACtory')
        
    def turnOffHeader(self):
        #Turn off the header for later execution (turn on only if required)
        self.scope.write("HEADER OFF")
        
    
    def initialize(self, ip_address_i, gpip_address_i, timeout_i):
        '''
            Todo after each change of any of the three attributes
        '''
        print('INITIALISATION OF IP/GPIB')
        self.rm = visa.ResourceManager()
        self.ip_address = ip_address_i
        print(self.ip_address)
        self.gpip_address = gpip_address_i
        print('GPIB ADDR: ', self.gpip_address)
        self.SWtimeout = timeout_i
        #Setting connectivity
        self.scope_tpcip = 'TCPIP0::%s::%s::INSTR' % (self.ip_address, self.gpip_address)
        print('TCPIP0::%s::%s::INSTR' % (self.ip_address, self.gpip_address))
        # Loading the scope
        self.scope = self.rm.open_resource(self.scope_tpcip)
        print('res. mang')
        #clear events
        self.scope.clear()
        #clear queue and wait until it is ready
        self.scope.query('*CLS;*OPC?')
        print("clearing the queue")
        # Checking if scope is ok
        print(self.scope.query('*IDN?'))
        print('initialized')
        #turn off the timeout
        self.scope.timeout  = None
        '''
        Also a library delivers an attribute timeout but it works in such a way that it exits from 
        session without saying it to the scope. So the scope gets frozen. If somebody would like to make
        it work it is necessary to play with error handling and dig deeper into underlying layers.
        None means that the timeout is infinite.
        Normally: self.scope.timeout = 44000 #[ms]
        '''
        

