# -*- coding: utf-8 -*-
from datetime import datetime # std library
import Setup
import time


class Debugging(object):
    
    '''
    This class contain tools useful for debugging and generally delivering feedback information to te user. 
    '''
 
    def __init__(self, setup):
        '''
                Creation of a debugging object
        '''
        self.setup = setup
        
        
        
        
    def takeScreenshot(self,setupObject):
        #set timeout to none to avoid cutting the connection
        #previousTimeout = setupObject.timeout
        #setupObject.timeout = None 
        
        
        fileSaveLocation = r'C:\Temp\\' # Folder on your PC where to save image
        print('1')
        self.setup.scope.write("HARDCopy:PORT FILE;")
        print('2')
        self.setup.scope.write("EXPort:FORMat PNG")
        print('3')
        # Set where the file will be saved on the scope's hard drive.  This is not
        # where it will be saved on your PC.
        self.setup.scope.write("HARDCopy:FILEName \"C:\\Temp.png\"")
        print('4')
        self.setup.scope.write("HARDCopy STARt")
        print('5')
        time.sleep(.1)
        # Read the image file from the scope's hard drive
        self.setup.scope.write("FILESystem:READFile \"C:\\Temp.png\"")
        #print('6')
        time.sleep(1)
        imgData = self.setup.scope.read_raw()
        #print('7')
        # Generate a filename based on the current Date & Time
        #dt = datetime.now()
        #fileName = dt.strftime("%Y%m%d_%H%M%S.png")
        
        # Generate a static filename
        fileName = "scopeScreen.png"
        
        # Save the transfered image to the hard drive of your PC
        print('1')
        imgFile = open(fileSaveLocation + fileName, "wb")
        print('2')
        imgFile.write(imgData)
        print('3')
        imgFile.close()
        print('4')
        # Delete the image file from the scope's hard drive.
        self.setup.scope.write("FILESystem:DELEte \"C:\\Temp.png\"")
        print('5')
        #revert the changes regarding timeout
        #setupObject.timeout = previousTimeout 
        print('6')
        #self.setup.scope.close()
        #self.setup.scope.open()
        print('7')