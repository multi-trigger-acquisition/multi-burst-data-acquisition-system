import matplotlib.pyplot as plt
import numpy as np
import time
'''
This class contains all the attributes related with new window in which an user can perform analysis and detailed plots
'''
class ExternalWindow(object):
    
    
        def __init__(self):
              
            '''
                Creation of an external window object
            '''
            # format of the plots names
            self.box = dict(facecolor='yellow', pad=5, alpha=0.2)
            #turning on interactive mode of matplotlib
            plt.ion()
            #create a new figure
            self.figMain = plt.figure('Expert mode plotting')
            #clear the figure to make sure there is nothing left in background (other plots)
            self.figMain.clf()
            #adjustment of location of the plots
            self.figMain.subplots_adjust(left=0.2, wspace=0.6)
            

            #create numpy series
            x1=np.linspace(0,5)
            x2=np.linspace(0,2)
            y1=np.cos(2 * np.pi * x1) * np.exp(-x1)
            y2=np.cos(2 * np.pi * x2)
            #containter for one-frame points
            self.frameFigure = frameFigure(x1,y1, self.figMain)
            #container for all samples (to plot the gradient)
            self.gradientFigure = gradientFigure(self.figMain)
            
            
            plt.show(block=False)
            self.figMain.canvas.draw()
            time.sleep(3)
        

    
    
class frameFigure(object):
    
        def __init__(self,xnp,ynp,figMain):
            #containter for one-frame points
            self.figMain=figMain
            self.ax1Frame = self.figMain.add_subplot(211)
            self.framePoints, = self.ax1Frame.plot(xnp, ynp)
            plt.pause(0.001)
            self.ax1Frame.set_xlim((-3, 3))
            self.ax1Frame.set_ylim((-3, 3))
            
        def update(self):
            self.figMain.canvas.update()
            self.figMain.canvas.flush_events()
            plt.pause(3)

class gradientFigure(object):
    
        def __init__(self,figMain):
            #container for all samples (to plot the gradient)
            self.figMain=figMain
            self.ax2Gradient = self.figMain.add_subplot(212)
            a=np.array([[1,2],[3,4],[5,6]])
            self.ax2Gradient  = self.figMain.gca()
            self.ax2Gradient.imshow(a)
            #self.gradientPointsx, self.gradientPointsy = self.ax2Gradient.plot(a)
            plt.pause(5)
            
            
            self.ax2Gradient.cla()
            
#            b=np.array([[1,4],[5,4],[2,6]])
#            self.ax2Gradient  = self.figMain.gca()
#            self.ax2Gradient.imshow(b)
#            plt.pause(0.001)
#            
#            time.sleep(3)
#            self.ax2Gradient.cla()
            #plt.close(self.ax2Gradient)
            #self.ax2Gradient.set_xlim((-3, 3))
            #self.ax2Gradient.set_ylim((-3, 3))
            
        #def update(self):
        #    self.figMain.canvas.update()
        #    self.figMain.canvas.flush_events()