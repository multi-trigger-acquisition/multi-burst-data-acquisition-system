import Setup as sp
import GeneralOsc as go
import matplotlib.pyplot as plt
import pylab
import time
import numpy
import matplotlib
from struct import unpack
import math
import os
from inspectorpy import InspectorCom
import datetime

class acquisitionObject(object):
    
    def __init__(self, setup, acquisitionName, channelNo, generalOscSetObject,ch2_beam):
        '''
                Creation of a source object
        '''
        self.generalOscSetObject = generalOscSetObject
        self.setup = setup
        self.acquisitionName = acquisitionName
        self.channelNo = channelNo
        self.ch2_beam = ch2_beam
        self.data = None 
        #date of the last acquisition
        self.date = 0

    
   



    def saveToFile(self,name,modePosOff,modeSc):
        #modeSc - change this variable if you want to store data in unscalled form
        #modeSc = True
        
        '''
        Storing the data together with metadata. In order to choose the representation of the
        data please change the variable modeSc (scalling mode). The file will contain either
        unscalled version of data together with metadata (scalling factor, offset etc.) or scalled
        version where the data is explicitly scalled to a floating point number expressed as a Volt.
        '''
        # In order to make it more reliable please update the values directly from the scope, not from Inspector panel.
        
        #self.yoffset = self.setup.scope.ask('wfmoutpre:yoff?')        
        #self.ymult = self.setup.scope.ask('wfmoutpre:ymult?')
        #self.yzero = self.setup.scope.ask('wfmoutpre:yzero?')
        #self.yunit = self.setup.scope.ask('WFMOutpre:YUNit?')
        #self.numberofpoints =self.setup.scope.ask('wfmoutpre:nr_pt?')
        #self.xincrement = self.setup.scope.ask('wfmoutpre:xincr?')
        #self.xzero = self.setup.scope.ask('wfmoutpre:xzero?')
        #self.PT_OFf = float(self.setup.scope.ask("WFMOutpre:PT_OFf?"))
        #name = inspector.getVariable('$name')
        
        #Turn on the header (unnecessary, the header is turned off right after starting the application)
        #self.setup.scope.write("HEADER ON")
        
        thefile = open('savedData/%s_data.txt' %name, 'w')
        
        
        thefile.write("%s\n" % self.date)
        
        if(modeSc==False):
            #modeSc==False
            thefile.write("%s\n" % self.yoffset)
            thefile.write("%s\n" % self.ymult)
            thefile.write("%s\n" % self.yzero)
            thefile.write("%s" % self.yunit)
            thefile.write("%s\n" % self.numberofpoints)
            thefile.write("%s\n" % self.xincrement)
            thefile.write("%s\n" % self.xzero)
            thefile.write("%s\n" % self.PT_OFf)
        
        if(modeSc==True):
            #modeSc==True
            if(modePosOff==True):
                for item in self.data:
                    point = self.yzero + self.ymult*(item-self.yoffset)
                    thefile.write("%s\n" % point)
            else:
                for item in self.data:
                    point = self.ymult*(item-self.yoffset)
                    thefile.write("%s\n" % point)
                
        else:
            #modeSc==False
            for item in self.data:
                thefile.write("%s\n" % item)
        
        #Turn off the header
        #self.setup.scope.write("HEADER OFF")
        
        print("saveToFile")

    
    def fillDataIfNotYetAcquired(self):
        #force scope to give data from the scope that is currently in the buffer
        if(self.data == None):
            print("gathering data from the scope buffer")
            start = time.time()
            self.resolution  = int(self.setup.scope.ask('HORizontal:RESOlution?'))
            self.setup.scope.write("DATa:STARt 1")
            self.setup.scope.write("DATa:STOP "+str(self.resolution))
            #  int, # record length of record waveform
            self.numberofpoints = int(self.setup.scope.ask('wfmoutpre:nr_pt?'))
            #equivalent with the following query
            #NR_Pt = int(self.setup.scope.ask("WFMOutpre:NR_Pt?"))
            
            
            #Make sure to transfer from first to last datapoint
            self.setup.scope.write("DATa:STARt 1") 
            self.setup.scope.write("DATa:STOP "+str(self.numberofpoints))
            #activate and select channel to read out
            #self.setup.scope.write("SELect:CH%i ON" 2)
            #self.setup.scope.write("DATa:SOUrce CH%i" %self.channel_idx )
            
            #extract wafeform scaling parameters
            #float, # Y zero value
            self.yzero = float(self.setup.scope.ask("WFMOutpre:YZEro?"))
            #'YMULT'  : float, # Difference between two y point
            #ymult is the scaling factor that is set by ch<x>:scale
            self.ymult = float(self.setup.scope.ask("WFMOutpre:YMUlt?"))
            self.yoffset = float(self.setup.scope.ask("WFMOutpre:YOFf?"))
            #yoffset is unscaled offset data that is set by the ch<x>:offset
            #        self.yoffset = float(self.setup.scope.ask("CH%d:OFFset?"%self.channelNo))
            #investigation of the str, # Y unit
            self.yunit = self.setup.scope.ask('WFMOutpre:YUNit?')
            #yzero is scaled position data that is set by ch<x>:position
            #absolute time value of the beginning of the waveform record
            self.xzero = float(self.setup.scope.ask("WFMOutpre:XZEro?"))
            #amount of time between data points
            self.xincrement = float(self.setup.scope.ask("WFMOutpre:XINcr?"))
            #float, # Y offset
            self.PT_OFf = float(self.setup.scope.ask("WFMOutpre:PT_OFf?"))
            tr = numpy.array((self.setup.scope.query_binary_values('CURV?', datatype='B', is_big_endian=False)), dtype=numpy.uint8)
            rawdata = numpy.frombuffer(tr, dtype=numpy.uint8, offset = 0)
            #print(int(tr.size()))
                       
            
            end = time.time()
            print("time of data acquisition: ")
            print(end - start)
            self.data = rawdata
            
       
        
    def plotFrame(self,modePosOff,NoOfFrame):
        #update data from the scope to the panel
        self.fillDataIfNotYetAcquired()
        
        ##### PLOT WITH CORRECT SCALLING ONLY IN THE PLOT
        x = numpy.linspace(0, self.numberofpoints, self.numberofpoints)
        #slight change has to be done as the scope automatically decreases the resolution when the number of frames exceed the 
        #capabilities of the scope for a given resolution
        startPoint = (NoOfFrame-1)*self.numberofpoints
        endPoint = (NoOfFrame-1)*self.numberofpoints + self.numberofpoints
        y = self.data[startPoint:endPoint]
        print(x.size)
        print(y.size)
        
        # setup figures
        fig2 = plt.figure('figure 2')
        plt.clf()
        #ax1 = fig.add_subplot(121)
        ax2 = fig2.add_subplot(122)
        # plot two identical plots
        #ax1.plot(x, y)
        ax2.plot(x, y) #self.yzero + self.ymult*(y-self.yoffset)
        ax2.set_ylim((0,255))
        ticks_x = matplotlib.ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(self.xzero+self.xincrement*(float(x)-self.PT_OFf)))
        ax2.xaxis.set_major_formatter(ticks_x)
        #  dataByte = uint8((data - ymin) / yScaleF);
        # voltage[i]=ymult*raw_data[i]+yzero
        if(modePosOff==True):
            ticks_y = matplotlib.ticker.FuncFormatter(lambda y, pos: '{0:g}'.format(self.yzero + self.ymult*(y-self.yoffset)))
        else:
            ticks_y = matplotlib.ticker.FuncFormatter(lambda y, pos: '{0:g}'.format(self.ymult*(y-self.yoffset)))
            
        
        #ticks_y = map(int16, ticks_y)
        #ticks_y = matplotlib.ticker.FuncFormatter(lambda y, pos: '{0:g}'.format((y)/15))
        #ticks_y = matplotlib.ticker.FuncFormatter(lambda y, pos: '{0:g}'.format(y*self.ymult-self.yzero))
        ax2.yaxis.set_major_formatter(ticks_y)
        ax2.set_xlabel("[s]")
        ax2.set_ylabel('[V]')
        plt.savefig('foo.png', bbox_inches='tight')
        plt.close(fig2)
        
    def AdvancedPlotFrame(self, modePosOff,NoOfFrame,frameFigure):
        '''
        The function is responsible for plotting data in the pop up window. The plot is supposed to be of good quality 
        and gives possibility to zoom, unzoom parts of the data.
        It operates on the global data, which is in fact rawdata, unscalled version received directly from the scope
        '''  
        #update data from the scope to the panel
        self.fillDataIfNotYetAcquired()

        # the first point of the frame
        startPoint = (NoOfFrame-1)*self.numberofpoints
        #the last point of the frame + 1
        endPoint = (NoOfFrame-1)*self.numberofpoints + NoOfFrame*self.numberofpoints
        
        tcol = self.xzero+self.xincrement*(numpy.array(range(self.numberofpoints),dtype=float)-self.PT_OFf)
        print("modePosOff " , modePosOff)
        if(modePosOff==True):
            trace = self.yzero + self.ymult*(self.data[0:self.numberofpoints]-self.yoffset)
        else:
            trace = self.ymult*(self.data[startPoint:endPoint]-self.yoffset)
        
        frameFigure.framePoints.set_data(tcol, trace)
        
        
        #a handle for datapoints, we can interactively manipulate with the points
        #framePoints = frameFigure.ax1Frame.plot(tcol, trace)
        #set the limit for x axis
        frameFigure.ax1Frame.set_xlim(tcol[0], tcol[self.numberofpoints-1])

        #Move the axis to simulate the scope window
        Vposition= float(self.setup.scope.ask('CH%d:POSition?' %self.channelNo))
        #Vscale = float(self.setup.scope.ask(('CH%d:SCAle?' %self.channelNo)))
        Vscale = self.ch2_beam.verticalScale
        print('scale from the insp ',Vscale )
        frameFigure.ax1Frame.set_ylim(((-5-Vposition)*Vscale, (5-Vposition)*Vscale))
        #Set the labels of units
        frameFigure.ax1Frame.set_xlabel('Time (sec)')
        frameFigure.ax1Frame.set_ylabel('Voltage (V)')
        
        #plt.plot(tcol,trace)
        #plt.ylabel('Voltage (V)')
                
        # Don't mess with the limits!
        #plt.autoscale(False)
        #plt.xlabel('Time (sec)')
        #plt.show()
        
        #to easly update the plot points
        
        frameFigure.ax1Frame.draw_artist(frameFigure.ax1Frame.patch)
        frameFigure.ax1Frame.draw_artist(frameFigure.framePoints)
        
        frameFigure.update()
        
        
        
        print('Plot generated.')
        
        
    def plotGradient(self, modePosOff):
        '''
        Plotting gradient directly in the panel. The result will not be detailed and will be optimised for fast refresh.
        Intended to be used for position correction for the future measurement.
        '''
        #update data from the scope to the panel
        self.fillDataIfNotYetAcquired()
        
        #the number of points included in one frame
        print('no of points ' , self.numberofpoints)
        #the type of the points
        print(type(self.numberofpoints))
        
        self.numberofpoints = int(self.setup.scope.ask('wfmoutpre:nr_pt?'))
        self.framecnt = int(self.setup.scope.ask("WFMOutpre:NR_FR?"))
        #self.framecnt = self.generalOscSetObject.fastFrameFrameCount
        #reshapedData=rawdataInt.reshape(20 , 5000)
        reshapedData=self.data.reshape(self.framecnt, self.numberofpoints)
        print('numberofpoints ',self.numberofpoints)
        print('framecnt ',self.framecnt)
        fig =plt.figure('inner plot 1')
        plt.clf()
        plt.pause(1)
        ###fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_aspect('equal')
        plt.imshow(reshapedData, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom')
        plt.colorbar()
        plt.tight_layout()
        #plt.show()
        fig.savefig('gradient.png')   # save the figure to file
        plt.close(fig)    # close the figure        
        plt.pause(1)
        print("The plot has been stored and shown on the panel")
        
    
    def AdvancedPlotGradient(self, modePosOff, gradientFigure):
        print("hs")
        #update data from the scope to the panel
        self.fillDataIfNotYetAcquired()
        
        #the number of points included in one frame
        print('no of points ' , self.numberofpoints)
        #the type of the points
        print(type(self.numberofpoints))
        
        self.framecnt = int(self.setup.scope.ask("WFMOutpre:NR_FR?"))
        #self.framecnt = self.generalOscSetObject.fastFrameFrameCount
        
        reshapedData=self.data.reshape(self.framecnt, self.numberofpoints)
        print('numberofpoints ',self.numberofpoints)
        print('framecnt ',self.framecnt)
        gradientFigure.ax2Gradient.cla()
        
        gradientFigure.ax2Gradient.imshow(reshapedData, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom')
        
        #plt.pause(1)    
        #time.sleep(3)
        
        plt.pause(5)
        print('Plot generated.')
        

    def acquireData(self,timeout):
        print("acquireData")
        
        '''
        Extract waveform scaling parameters. As the returned values are strings. 
        They need to be converted to numerical data types in order to use them 
        to scale, thus typecasting.
        '''
        
        startmd = time.time()
        self.resolution  = int(self.setup.scope.ask('HORizontal:RESOlution?'))
        self.setup.scope.write("DATa:STARt 1")
        self.setup.scope.write("DATa:STOP "+str(self.resolution))
        # record length of record waveform
        self.numberofpoints = int(self.setup.scope.ask('wfmoutpre:nr_pt?'))

        
        
        #Make sure to transfer from first to last datapoint
        #It is important, otherwise the returned number of records
        #might not be the same as requested.
        self.setup.scope.write("DATa:STARt 1") 
        self.setup.scope.write("DATa:STOP "+str(self.numberofpoints))
        #activate and select channel to read out
        #self.setup.scope.write("SELect:CH%i ON" 2)
        #self.setup.scope.write("DATa:SOUrce CH%i" %self.channel_idx )
        self.numberofframes = int(self.setup.scope.ask('HORizontal:FASTframe:COUNt?'))
        self.setup.scope.write("data:framestart "+str(1))
        self.setup.scope.write("data:framestop " +str(self.numberofframes) )
        
        
        #extract wafeform scaling parameters
        #float, # Y zero value
        self.yzero = float(self.setup.scope.ask("WFMOutpre:YZEro?"))
        print("yzero 1 " , self.yzero)
        #'YMULT'  : float, # Difference between two y point
        #ymult is the scaling factor that is set by ch<x>:scale
        self.ymult = float(self.setup.scope.ask("WFMOutpre:YMUlt?"))
        self.yoffset = float(self.setup.scope.ask("WFMOutpre:YOFf?"))
        print(self.yoffset)
        #yoffset is unscaled offset data that is set by the ch<x>:offset
        #self.yoffset = float(self.setup.scope.ask("CH%d:OFFset?"%self.channelNo))
        #investigation of the str, # Y unit
        self.yunit = self.setup.scope.ask('WFMOutpre:YUNit?')
        #yzero is scaled position data that is set by ch<x>:position
        #absolute time value of the beginning of the waveform record
        self.xzero = float(self.setup.scope.ask("WFMOutpre:XZEro?"))
        #amount of time between data points
        self.xincrement = float(self.setup.scope.ask("WFMOutpre:XINcr?"))
        #float, # Y offset
        self.PT_OFf = float(self.setup.scope.ask("WFMOutpre:PT_OFf?"))
        
        endmd = time.time()
        timeMetadata = endmd - startmd
        
        #read wafeform preamble for debugging purposes
        #preamble = self.ask("WFMOutpre?")
        #print(preamble)
        
        #setting the source of data - channel
        self.setup.scope.write('DAT:SOUR CH%d' % (self.channelNo))   
        print('DAT:SOUR CH%d' % (self.channelNo))
        #set single acquisition
        self.setup.scope.write('ACQUIRE:STOPAFTER SEQUENCE')
        print('ACQUIRE:STOPAFTER SEQUENCE')
        #Start the timer
        
        
        #turn on the acquisition system
        self.setup.scope.write('acquire:state 1')
        
        #/* Enable the status registers */
        #DESE 1
        #self.setup.scope.write('DESE 1')
        #*ESE 1
        #self.setup.scope.write('*ESE 1')
        #*SRE 0
        #self.setup.scope.write('*SRE 0')
        
        #Some instructions for handling data acquisition cause deadlock. 
        
        #automatic way of timing out - does not always work
        #scope.write('ACQUIRE:STATE ON')
        #wait for data -  Using *OPC? query waits until the instrument finished the acquisition
        #self.setup.scope.query('*OPC?')
        #self.setup.scope.write('*WAI')
        
        '''
        Manual way of timing out. Creating a value to return true if it was a successful acquisition or false otherwise.
        This mechanism prevents from deadlck that was often caused by *OPC?, *WAI statements.
        The deadlock appeared on the side of keysight device (GPIB->LAN connnector) not the oscilloscope.
        '''
        success = True
        
        expectedTime = timeout
        start_time = time.time()
        print(start_time)
        while True:
            elapsed_time = time.time() - start_time
            if(elapsed_time <= expectedTime):
                #exit from the loop if the data is in
                if(int(self.setup.scope.ask("BUSY?")) == 0):
                    print("break")
                    break
                time.sleep(.2)
            else:
                self.setup.scope.clear()
                print("break and clear")
                success = False
                break
        
        # storing the content of the scope buffer in numpy array
        # the end of the speed of the connection timing
        start = time.time()
        
        
        
        tr = numpy.array((self.setup.scope.query_binary_values('CURV?', datatype='B', is_big_endian=False)), dtype=numpy.uint8)
        end = time.time()
        print("time of data acquisition: ")
        print(end - start + timeMetadata)
        
        '''
        Normaly a user should care about first few bytes of metadata but in this case the manual shifting can be omited 
        as the operation is automatically done by pyvisa library (so offset is just 0).
        rawdata = numpy.frombuffer(b, dtype=numpy.uint16, offset = offsetByte)
        offsetByte =  int(tr[1])+2
        '''

        rawdata = numpy.frombuffer(tr, dtype=numpy.uint8, offset = 0)
        '''
        Checking if the amount of data is correct. Sometimes the scope downsize the amount if the user will ask for more dataframes 
        than it is possible - the maximum is 4395 frames with the resolution of 500 (the resolution is also downsized if inconsistent
        with horizontal scale and the required number of fast frames - this problem is mitigated when plotting). The data is returned in
        the raw format so that the user can choose in which way to store it.
        '''
        print('len ', len(rawdata))
        
        self.data = rawdata
        print("Scalling information as well as the data have been acquired")
        
        
        # Making timestamp of the acquisition
        #self.date = datetime.datetime.now()
        # acquiring timestamp for only one - first frame, it is possible to acquire timestamp for each frame e.g. 4,1
        self.date = self.setup.scope.ask('HORIZONTAL:FASTFRAME:TIMESTAMP:ALL:CH%d? 1,1' % (self.channelNo) )[3:29]

      
        
        return self.data, success

    def plotProgramWithTriggers(self,inspector):
        
        #style used in the plot below
        font = {'family': 'serif',
        'color':  'darkred',
        'weight': 'normal',
        'size': 16,
        }
        
        
        noOfTriggers = 16
        speedOfLight = 299.792458*10**6  # m/s
        bending_radius = 70.09  # m
        
        '''
        Plot a figure combining magnetic field and trigger dependency in new window
        '''
        
        
        pathToFieldData = os.path.abspath('cavityprograms/PStoLHCMomentum.csv')
        #autstrip - get rid of whitespaces
        data = numpy.genfromtxt(pathToFieldData,skip_header=3,delimiter=',',usecols=(0,1),dtype=float,autostrip=True)
        #np.isnan(a) returns a similar array with True where NaN, False elsewhere. .any(axis=1) 
        #reduces an m*n array to n with an logical or operation on the whole rows, ~ inverts 
        #True/False and a[  ] chooses just the rows from the original array, which have True within the brackets.
        filteredData = data[~numpy.isnan(data).any(axis=1)]
        #before , be cautious with the matrices' multiplication
        magneticInductionInTesla= filteredData[:,1]/ 1e4
        Momentum = magneticInductionInTesla*speedOfLight*bending_radius
        #Momentum [eV/C]
        x = filteredData[:,0]
        y = Momentum
        
        
        
        figMom = plt.figure('Momentum in time')
        plt.clf()
        axMom = figMom.add_subplot(111)
        
        
        axMom.plot(x, y, 'k')
        
        
        axMom.set_title('Momentum in time', fontdict=font)
        axMom.text(2, 0.65, r'$B*rho=P/q$', fontdict=font)
        axMom.set_xlabel('time (ms)', fontdict=font)
        axMom.set_ylabel('P (eV/c)', fontdict=font)

        '''
        plot lines of stages together with stages
        '''
        #plt.axvline(x=100)
        plt.text(100,3e10,'Flat bottom',rotation=0)
        plt.axvline(x=170)
        plt.text(170,2e10,'Injection',rotation=90)
        #plt.axvline(x=1500)
        plt.text(1500,3e10,'Plateau',rotation=0)
        plt.axvline(x=1830)
        plt.text(1830,2e10,'Trisplit',rotation=90)
        plt.axvline(x=2023)
        plt.text(2023,2e10,'Transition',rotation=90)
        plt.axvline(x=2050)
        plt.text(2050,2e10,'Landau',rotation=90)
        plt.axvline(x=2600)
        plt.text(2600,2e10,'Sync SPS',rotation=90)
        plt.axvline(x=2710)
        plt.text(2710,2e10,'Twosplit 1-2',rotation=90)
        plt.axvline(x=2793)
        plt.text(2793,2e10,'Twosplit 2-4',rotation=90)
        plt.axvline(x=2849)
        plt.text(2849,2e10,'Bunch rotation',rotation=90)
        #plt.axvline(x=2500)
        plt.text(2500,3e10,'Flat top',rotation=0)
        #plt.show()
        '''
        add the time periods of the triggers to the plot
        '''
        axTrig = axMom.twinx()
        
#        begin = numpy.ones(16)
#        end =   numpy.ones(16)

        begin = numpy.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        end =   numpy.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])

        for x in range(1, noOfTriggers+1):
            if(inspector.getVariable('$S%dMEnable'%(x))==True):
                turnDist = inspector.getVariable('$S%dMTurnDist'%(x))
                CTime = inspector.getVariable('$S%dMCTime'%(x))
                NoOFFrames = inspector.getVariable('$S%dMNoFrames'%(x))
                CTimeDelay = inspector.getVariable('$S%dMCtimeDelay'%(x))
                #start time in [ms]
                startTime = CTime + (CTimeDelay*1e-7)*1000
                #end time in [ms]
                periodOneTurn=22e-7
                endTime = startTime + periodOneTurn*turnDist*NoOFFrames*1000
                begin[x-1]=startTime
                end[x-1]=endTime
            else:
                begin[x-1]=0
                end[x-1]=0


        event = ["Event {}".format(i) for i in range(len(begin))]
        #axMom.plot(x, event, 'k')
        
        #axTrig.set_title('', fontdict=font)
        axTrig.barh(range(len(begin)),  end-begin, left=begin, color='red',alpha = 0.7)
        axTrig.set_yticks(range(len(begin)), event)
        #axTrig.set_alpha(0.5)
        
        
        figMom.tight_layout()
        plt.show()
        plt.pause(2)
        plt.close(figMom)

        
        
        
        