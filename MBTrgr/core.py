import GeneralOsc as gn
import InputSource as isrc
import Setup as sp
import TriggerDevice as trgr
import TrigOsc as tos
import Acquisition as acq


#%%
'''
1. Establish connection to triggering device
'''
cycleName = "CPS.USER.LHC4"
#cycleName = "CPS.USER.MD4"
triggerList = trgr.establishConnectivity(cycleName)

#%%
'''
2. Clear all the other users
'''
trgr.disableAllTriggers()
'''
#%%
3. Enable only the choosen ones
'''
trigger1 = triggerList[0]
trigger1.enableTrigger()
trigger1.changeCTime(169)
trigger1.changeCTimeDelay(10160)
trigger1.changeNumberFrames(2)
trigger1.changeTurnDistance(1)
'''
4. Set some initial parameters for oscilloscope
'''
# Select IP and gpip address of the scope
ip_address = '128.141.96.95'
#Attention. the gpib0,3 works but e.g. gpib0,2 doesn't
gpip_address = 'gpib0,3'
#timeout
timeout = 4000 #[s]
setupObject = sp.setupObject(ip_address, gpip_address, timeout)
#%%
'''
5. Choose ports for the beam and trigger
connect beam (ch2)
connect trigger (ch1)
channelObject(self, channelName, noOfChannel):
'''
couplingLst = ['DC','AC']

#Trigger channel initialisation
triggerChannel = 1
#trigger vertical position
triggerVPosition = -2
#trigger termination
triggerTermination = '1E+6'
#vertical scale (2V)
triggerVScale = '14E-1'
#select trigger coupling
triggerCoupling = couplingLst[0]
ch1_trigger = isrc.channelObject('trigger',triggerChannel, setupObject, triggerTermination, triggerVPosition, triggerVScale, triggerCoupling)
#%%
#Beam channel initialisation
# Select data source
beamChannel = 2
# select termination
beamTermination = '1E+6'
#beam v position
beamVPosition = 1
#vertical scale [e.g. 100mv per division]
beamVScale = '5E-01'
#beam coupling
beamCoupling = couplingLst[1]
ch2_beam = isrc.channelObject("beam", beamChannel, setupObject, beamTermination, beamVPosition, beamVScale, beamCoupling)
#%%
'''
6. set trigger settings on the oscilloscope
'''
#select trigger slope
triggerSlopeLst = ['RISe','FALL']
triggerSlope = triggerSlopeLst[0]
#select trigger coupling 
triggerCouplingLst = ['DC','AC','HFRej']
triggerCoupling = triggerCouplingLst[0]
#select triggel level (<NR3> - fp with exponent)
triggerLevelLst = ['ECL','TTL']
triggerLevel = triggerLevelLst[1]
#Trigger mode
triggerModeList = ['AUTO','NORMal']
triggerMode = triggerModeList[1]
#Trigger horizontal position
#0-100 (integer)
triggerHPosition = 24
#in the method the coupling can have different values from AC & DC, also HFRej, LFRej and NOISErej
triggerOscSetCHobj = tos.triggerOscSettingsCH(setupObject,ch1_trigger,triggerSlope,triggerCoupling,triggerLevel,triggerMode,triggerHPosition)
#%%
'''
7. setting the general settings
'''
#GENERAL VIEW
##Main time base
#Horizontal scale (2 us per divison)
horizontalMainScale = '2.0E-7' 
#horizontal main position
#it has an influence on trigger position!
horizontalMainPos = '6E+1'#50
#sample rate, attention! has influence on record length
#1.25GS/s

mainSampleRate = '2.5E9'
##position on the display
#horizontal scale is equivalent to horizontal main scale
#horizontalScale
#horizontal resolution e.g. 5000 = 5000 data points will be acquired each record
horizontalResolution = 5000
#horizontal position -10% ov waveform is to the left of the screen center
horizontalPosition = 50
#horizontal recordlength -sets record length to the number of data points in each frame 
#atention! has influence on sample rate
horizontalRecordLen =  5000
##FastFrame - memory segmentation, captures a series of triggered acquisitions with minimal intervening time.
#reference frame - 2nd frame
fastFrameReferenceFrame = 1
#frame count - number of frames to acquire
fastFrameFrameCount = 2
#frame length
fastFrameFrameLength = 5000
generalOscSet = gn.generalOscSetObject(setupObject,horizontalMainScale, horizontalMainPos, mainSampleRate, horizontalResolution, horizontalPosition, horizontalRecordLen, fastFrameReferenceFrame, fastFrameFrameCount, fastFrameFrameLength)
#%%
'''
8. Running acquisition
'''
newAcquisition = acq.acquisitionObject(setupObject,'testAcq',2,generalOscSet)
newAcquisition.singleAcquisition()
'''
10. Return to default setings
'''
#setupObject.rstFactorySettings()