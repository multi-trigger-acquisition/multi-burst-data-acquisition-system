# -*- coding: utf-8 -*-
'''
Script to set the Multi burst triggers with pyjapc

author: pakozlow, alasheen
'''

from pyjapc import PyJapc
import logging
import sys
#import os
import numpy as np
import matplotlib.pyplot as plt
#from scipy.constants import e,c 

#LHC25#72b
#cycleName = "CPS.USER.LHC1"

#CPS.USER.SFTPRO2	MTE_200_5t
#cycleName = "CPS.USER.SFTPRO2"


#LHC_INDIV_ForB
#cycleName = "CPS.USER.MD4"

#LHCINDIV_2018
cycleName = "CPS.USER.LHCINDIV"

#cycleName = "CPS.CYCLE.whatever"

logging.root.addHandler(logging.StreamHandler(sys.stdout))
japc = PyJapc(selector=cycleName)


#%% Magnetic field

#BRef = japc.getParam("PR.SCBFC/FunctionList#value")
#
#timeBField = BRef[0][0]
#BField = BRef[0][1]
#
#plt.figure('Cycle timings')
#plt.clf()
#plt.plot(timeBField, BField)


    

#%% Trigger object


    
class triggerObject(object):
    
    def __init__(self, triggerNumber, japc):
        '''
        
        Future dev> 
        Need to evaluate the limits of the parameters like numberFrames
        '''
        
        self.deviceNameStart = 'PAX.S%dMBURST'%(triggerNumber)
        
        self.deviceNameTurnDistance = 'PAX.NT%dMBURST'%(triggerNumber)
        
        self.deviceNameNumberFrames = 'PAX.E%dMBURST'%(triggerNumber)
        
        self.japc = japc

    @property
    def triggerEnabled(self):
        
        return self.japc.getParam(self.deviceNameStart+'/OutEnable#outEnabled')
    
    @property
    def cTime(self):
        
        return self.japc.getParam(self.deviceNameStart+'/Payload#payload')
        
    @property
    def cTimeDelay(self):
        '''
        10 MHz clock !! 0.1 us
        Need to calibrate this delay so that the bunch is centered on the trigger
        '''
        
        return self.japc.getParam(self.deviceNameStart+'/Delay#delay')
        
    @property
    def numberFrames(self):

        return self.japc.getParam(self.deviceNameNumberFrames+'/Delay#delay')
    
    @property
    def turnDistance(self):

        return self.japc.getParam(self.deviceNameTurnDistance+'/Delay#delay')
    
    
    def enableTrigger(self):
        
        self.japc.setParam(self.deviceNameStart+'/OutEnable#outEnabled', 1)
        self.japc.setParam(self.deviceNameTurnDistance+'/OutEnable#outEnabled', 1)
        self.japc.setParam(self.deviceNameNumberFrames+'/OutEnable#outEnabled', 1)
        
    def disableTrigger(self):
        
        self.japc.setParam(self.deviceNameStart+'/OutEnable#outEnabled', 0)
        self.japc.setParam(self.deviceNameTurnDistance+'/OutEnable#outEnabled', 0)
        self.japc.setParam(self.deviceNameNumberFrames+'/OutEnable#outEnabled', 0)
        
    def changeCTime(self, newCTime):
        
        self.japc.setParam(self.deviceNameStart+'/Payload#payload', newCTime)  
        self.japc.setParam(self.deviceNameTurnDistance+'/Payload#payload', newCTime)
        self.japc.setParam(self.deviceNameNumberFrames+'/Payload#payload', newCTime)
        
    def changeCTimeDelay(self, newCTimeDelay):
        
        self.japc.setParam(self.deviceNameStart+'/Delay#delay', newCTimeDelay)  
        
    def changeNumberFrames(self, newNumberFrames):
        
        self.japc.setParam(self.deviceNameNumberFrames+'/Delay#delay', newNumberFrames)  
    
    def changeTurnDistance(self, newTurnDistance):
        '''
        Warning: changes the time of the very first trigger pulse !!
        '''
        
        self.japc.setParam(self.deviceNameTurnDistance+'/Delay#delay', newTurnDistance)  
        
        
### General functions

def initializeTriggers(japc, nTriggers=16):
    
    triggerList = []

    for indexTrigger in range(nTriggers):
        triggerList.append(triggerObject(indexTrigger+1, japc))
        
    return triggerList
    
    
def disableTriggers(triggerList):
    
    for trigger in triggerList:
        trigger.disableTrigger()
        
def returnAllCycles():
    
    return PyJapc(selector=None, incaAcceleratorName='PS').getParam('PS.INFO/Cycles#list')

def returnAllUsers():
    
    return PyJapc(selector=None, incaAcceleratorName='PS').getParam('PS.INFO/LsaContextMapping#users')

def disableAllTriggers():
    
    listOfUsers = returnAllUsers()
    
    for user in listOfUsers:
        japcUser = PyJapc(user)
        triggerList = initializeTriggers(japcUser)
        disableTriggers(triggerList)


# Initialize all the triggers
triggerList = initializeTriggers(japc)
trigger1 = triggerList[0]
#trigger1.enableTrigger()


#%% TEST ZONE -----------------------------------------------------------------


#%% Start trigger time

#nTriggers = 16
#
#triggerEnabledArray = np.zeros(nTriggers)
#startTimeTrigger = np.zeros(nTriggers)
#
#for indexTrigger in range(nTriggers):
#    triggerEnabledArray[indexTrigger] = japc.getParam(
#            'PAX.S%dMBURST/OutEnable#outEnabled' %(indexTrigger+1))
#    
#    startTimeTrigger[indexTrigger] = japc.getParam(
#            'PAX.S%dMBURST/Payload#payload' %(indexTrigger+1))
#
#
#

#%% Disabling trigger
#
#def enableTrigger(triggerNumber):
#
#    japc.setParam('PAX.S%dMBURST/OutEnable#outEnabled' %(triggerNumber), 1)
#    
#def disableTrigger(triggerNumber):
#
#    japc.setParam('PAX.S%dMBURST/OutEnable#outEnabled' %(triggerNumber), 0)
#
#def shiftLeft(triggerNumber):
#    japc.setParam('PAX.S%dMBURST/Payload#payload' %(triggerNumber), 204)




#%% This is kind of exact measure roller
#backup = japc.getParam('PAX.S%dMBURST/Delay#delay' %(8))
#japc.setParam('PAX.S%dMBURST/Delay#delay' %(8),10010)

#%% this is a general measure roller (probably )
#backup2  = japc.getParam('PAX.S%dMBURST/Payload#payload' %(8))
#


