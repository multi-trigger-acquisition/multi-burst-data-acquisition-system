import Setup as sp

#possible values
#the sample rate should be changed automatically
#The other parameters can be choosen by user

'''
The scales are available for DPO7254
'''
scales = {
    #scale
    '2E-8' :{
        #recordLength : {'sample rate':'duration'}
        '10000' : {'5E9' : '2E-7' },
    },
    '2.5E-8' :{
        #recordLength : {'sample rate':'duration'}
        '500000000' : {'4E10' : '2.5E-8' },
    },
    '1.25E-5' :{
        '500000': {'4E10' : '1.25E-5' },
    },
    '5E-8' :{
        '50000': {'2E10' : '2.5E-6' },
        '25000': {'4E10' : '2.5E-6' },
    },
    '1.25E-6' : {
        '400000': {'4E10'  : '1.25E-6'},
        '50000' : {'4E10' : '1.25E-6' },
        '5000' : {'4E10' : '1.25E-6' },
        '500'  : {'4E10' : '1.25E-6' },
    },
                            
     
}


'''
The scales are available for TDS5104 
'''
#scales = {
#    #scale
#    '2.0E-5' :{
#        #recordLength : {'sample rate':'duration'}
#        '500' : {'2.5E9' : '2E-4' },
#        '250' : {'2.5E9' : '1E-4' },
#        '50'  : {'2.5E9' : '2E-5' },
#    },
#    '4.0E-5' :{
#        '500': {'1.25E9' : '4E-4' },
#        '250': {'1.25E9' : '2E-4' },
#        '50' : {'1.25E9' : '4E-5' },
#    },
#    '8.0E-5' : {
#        '2000': {'2.5E9'  : '8E-4' },
#        '500' : {'6.25E8' : '8E-4' },
#        '250' : {'6.25E8' : '4E-4' },
#        '50'  : {'6.25E8' : '8E-5' },
#    },
#    '1.0E-6' : {
#        '1000000': {'1.25E9'  : '1E-11' },
#    },
#    '2.0E-6' : {
#        '1000000': {'2.5E9'  : '2E-11' },
#    },
#    '4.0E-7' :{
#        '10000' : {'2.5E9' : '4E-10' },
#        '5000' : {'1.25E9' : '8E-10' },
#        '2500': {'6.25E8' : '1.6E-8'},
#        '500': {'1.25E8' : '8E-8'},
#    },
#    '2.0E-7' :{
#        '50000' : {'2.5E9' : '4E-12' },
#        '25000' : {'2.5E9' : '8E-12' },
#        '10000': {'5.0E9' : '4E-10'},
#        '5000': {'2.5E9' : '4E-10'},
#    },
#    '8.0E-8' : {
#        '200000': {'2.5E9'  : '4E-12' },
#        '100000' : {'2.5E9' : '8E-12' },
#        '2000' : {'2.5E9' : '4E-10' }, 
#    },
#    '4.0E-8' : {
#        '100000': {'2.5E9'  : '4E-12' },
#        '500': {'1.25E9'  : '8E-10' },
#    },
#    '2.0E-8' : {
#        '50000': {'2.5E9'  : '4E-12' },
#    },
#    '1.0E-8' : {
#        '25000': {'2.5E9'  : '4E-12' },
#    },
#    '4.0E-9' : {
#        '10000': {'2.5E9'  : '4E-12' },
#    },
#                            
#     
#}
    
    
def returnListOfPossibleValues():
    listPosVal = []
    for the_key, the_value in scales.items():
        for the_key2, the_value2 in the_value.items():
            for the_key3, the_value3 in the_value2.items():
                listPosVal.append(the_key + ',' + the_key3 + ',' + the_key2)
    
    return listPosVal
                

    
#iterate through the nested dictionary
def recursive_items(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            yield (key, value)
            #yield from recursive_items(value)
        else:
            #print('else')
            yield (key, value)

#the method will check for consistency of choosen result and 
#alternatively propose a better solution
def verifyScaleAndRecordLength(generalOscSetObject, horizontalMainScale, mainSampleRate, horizontalResolution, horizontalRecordLen):
    #iterate through the tree-dictionary above
    found=0
    print('start')
    for scale in scales:
        print(scale)
        if scale==horizontalMainScale:
            print('match')
            recordLengths=scales[scale]
            for recordLength in recordLengths:
                print(recordLength)
                if recordLength == horizontalRecordLen:
                    print('found ')
                    found=1
                    break
            break
    isRecLenCorrect=0

    if(horizontalResolution == horizontalRecordLen):
        isRecLenCorrect = 1
        
    
    if(isRecLenCorrect==1 and found==1):
            print('success')
    else:  
        if(isRecLenCorrect==0):
            print('ERROR: horizontalResolution has to be equal to horizontalRecordLen')
            if(found==0):
                print('please change the values so that they fit to the possible combinations')
                print('Please find the possible combinations below:')
                for key, value in recursive_items(scales):
                    print('Scale : ', key, '\n' )
                    for value, innervalue in recursive_items(value):
                        print('[ recLength: ', value , ' ]'  )
                        for innervalue in recursive_items(innervalue):
                            print('| sample rate/duration',innervalue,'|')

                    
    
    
    
    

class generalOscSetObject(object):
    
    def __init__(self, setup, horizontalMainScale, horizontalMainPos, mainSampleRate, horizontalResolution, horizontalPosition, horizontalRecordLen, fastFrameReferenceFrame, fastFrameFrameCount, fastFrameFrameLength):
        '''
                Creation of a general oscillator settings object
        '''
        #a function - checker if mainSampleRate,horizontalMainScale and horizontalRecordLen fit
        # also the horizontal resolution have to equal the record length
        # if doesn't fit to the possible options, loop asking for other possibilities
        # also some advices on possible combinations would be very nice
        self.setup = setup
        
        self.changeHorizontalMainPos(horizontalMainPos)
        #self.mainSampleRate = mainSampleRate
        #self.horizontalResolution = horizontalResolution
        self.changeHorizontalPosition(horizontalPosition)
        self.changeHorizontalRecordLen(horizontalRecordLen)
        self.changeFastFrameReferenceFrame(fastFrameReferenceFrame)
        self.changeFastFrameFrameLength(fastFrameFrameLength)
        self.changeHorizontalMainScale(horizontalMainScale) 
        self.changeMainSampleRate(mainSampleRate)
        self.changeHorizontalResolution(horizontalResolution)
        self.changeFastFrameFrameCount(fastFrameFrameCount)
        #turn on fast frame
        self.setup.scope.write('horizontal:fastframe:state 1')
        print('The general scope settings have been changed')
        

    #MAIN independent on delay
    #HORIZONTAL
    def changeHorizontalMainScale(self, horizontalMainScale):
        ##horizontal main scale
        self.horizontalMainScale = horizontalMainScale
        self.setup.scope.write('HORizontal:MAIn:SCAle %s' % horizontalMainScale)
        
    def changeHorizontalMainPos(self, horizontalMainPos):
        #horizontal main position <NR3> 0 - 100%
        self.horizontalMainPos = horizontalMainPos
        self.setup.scope.write('HORizontal:MAIn:POSition %e' % horizontalMainPos)
        print('WARNING: change of the value has an influence on the position of trigger')
        
    def changeMainSampleRate(self, mainSampleRate):
        #sample rate - [no of samples per second] - the record length is automatically adjusted to maintain a constant number of
        #data points in each frame e.g. 125 MS/s : 125E5
        self.mainSampleRate = mainSampleRate
        self.setup.scope.write('HORizontal:MAIn:SAMPLERate %s' % mainSampleRate)
    
    ##position on the display
    def changeHorizontalResolution(self, horizontalResolution):
        #horizontal resolution - seths horizontal record length to the number of data points in each frame. The sample rate is 
        #automatically adjusted at the same time to maintain a constant time per division
        self.horizontalResolution = horizontalResolution
        self.setup.scope.write('HORizontal:RESOlution %s' % horizontalResolution)
        

        
    def changeHorizontalPosition(self, horizontalPosition):
        #horizontal position - equivalent with the 'main' HOR:POS if the delay mode is turned off
        self.horizontalPosition = horizontalPosition
        self.setup.scope.write('HORizontal:POSition %e' % horizontalPosition)
        
        
    def changeHorizontalRecordLen(self, horizontalRecordLen):
        #horizontal recordlength
        self.horizontalRecordLen = horizontalRecordLen
        self.setup.scope.write('HORizontal:RECOrdlength %d' % horizontalRecordLen)

        ##FastFrame       
    def changeFastFrameReferenceFrame(self, fastFrameReferenceFrame):
        #reference frame no is used to calculate time differences for each frame and displays those differences in the graticule
        self.fastFrameReferenceFrame = fastFrameReferenceFrame
        self.setup.scope.write('HORizontal:FASTfram:REF:FRAME %d' % fastFrameReferenceFrame)
        
    def changeFastFrameFrameCount(self, fastFrameFrameCount):
        #sets or returns fastframe frame count
        self.fastFrameFrameCount = fastFrameFrameCount
        self.setup.scope.write('HORizontal:FASTframe:COUNt %d' % fastFrameFrameCount)
        
    def changeFastFrameFrameLength(self, fastFrameFrameLength):
        #horizontal record length to the number of sample points per frame
        self.fastFrameFrameLength = fastFrameFrameLength
        self.setup.scope.write('HORizontal:FASTframe:LENgth %d' % fastFrameFrameLength)

