import visa
import matplotlib.pyplot as plt
import pylab
import numpy as np
import Setup as setp




#select trigger source (CH1)
# corresponding indices will be used 
triggerSrcLst = ['AUXiliary', 'CH1', 'CH2', 'CH3', 'CH4', 'LINE']
triggerSrc = triggerSrcLst[1]
#triggerCHInt=1


'''
The general class from which other subclasses will derive their main features.
For example AUXiliary input has different methods to perform than normal channels.

TODO: populate the attributes which are different for all types of input and make a distinction of them
'''
class inputSourceObject(object):
    
    def __init__(self, channelType, channelName, setupObject):
        '''
                Creation of a source object
        '''
        self.setupObject = setupObject
        self.channelType = channelType
        self.channelName = channelName




class channelObject(inputSourceObject):
    
    def __init__(self, channelName, setupObject):
        '''
                A channel 1 - 4 object. The name of the channel will be displayed on the scope.
                NoOfChannel variable corresponds to the number of the channel eg. channel 4 (CH4) -> 4
        '''
        channelTypeV = 'CH'
        inputSourceObject.__init__(self, channelTypeV,channelName, setupObject) 


    def initialisation(self,  noOfChannel, setupObject, termination, position, scale, coupling, CHOffset):
        self.setupObject = setupObject
        self.noOfChannel = noOfChannel
        self.CHOffset = float(CHOffset)
        self.fullGPIBName = 'CH%s' %(noOfChannel)
        print(noOfChannel)
        self.changeTermination(termination)
        print(termination)
        self.changeVerticalPosition(position)
        print(position)
        self.changeVerticalScale(scale)
        print(scale)
        self.changeCoupling(coupling)
        print(coupling)
        self.changeChannelOffset()
        
        '''
        for optimization purpose sniffing each channel is not automatically done.
        It prevents from detecting the trigger channel as another one to acquire data about.
        If the trigger channel is observed together with beam one, the maximum resolution
        is halved.
        '''
        
        #self.setupObject.scope.write('SELECT:CH%s ON' %(noOfChannel))
        #print('A channel :CH%s has been reinitialized' %(noOfChannel))
        
        # by default turn on termination on each channel - 50 ohm
        self.turnOnTermination50o()
        
        
    def turnOnTermination50o(self):
        # sets the internal termination for the given channel
        self.setupObject.scope.write('CH%s:TERmination 50.0E+0' %(self.noOfChannel))

    def turnOff(self):
        self.setupObject.scope.write('SELECT:CH%s OFF' %(self.noOfChannel))
        
    def turnOn(self):
        self.setupObject.scope.write('SELECT:CH%s ON' %(self.noOfChannel))

    def changeChannelOffset(self):
        '''
        The settable range of a channel offset is either +/-100 V, +/-10 V or
        +/-1.0 V, depending on the vertical scale factor.
        !!! 
        '''
        self.setupObject.scope.write('CH%s:OFFset %e' %(self.noOfChannel,self.CHOffset))
        #print('CH%s:OFFset?' %(self.noOfChannel))

    def changeCoupling(self, coupling):
        self.coupling = coupling
        self.setupObject.scope.write('CH%d:COUPling %s' % (self.noOfChannel, coupling))

    def changeName(self, channelName):
        #define name
        self.channelName = channelName
        self.setupObject.scope.write('CH%d:LABEL:NAMe %s' %(self.noOfChannel, channelName))

    def changeTermination(self, termination):
        #termination
        self.termination=termination
        self.setupObject.scope.write('CH%d:TERmination %s' %(self.noOfChannel, termination))

    def changeVerticalPosition(self, verticalPosition):
        #vertical position <-8;8>
        self.verticalPosition=verticalPosition
        self.setupObject.scope.write('CH%d:POSition %e' %(self.noOfChannel, verticalPosition))

    def changeVerticalScale(self, verticalScale):
        #Vertical Scale
        self.verticalScale=verticalScale
        self.setupObject.scope.write('CH%d:SCAle %s' %(self.noOfChannel, verticalScale))
        print('CH%d:SCAle %s' %(self.noOfChannel, verticalScale))
        
    
    
    
    

'''
class auxiliaryObject(object):
    
    def __init__(self, channelName):
        ''''''
                An auxiliary input object
        ''''''
        self.channelTypeV = 'AUXiliary'
        inputSourceObject.__init__(self, channelTypeV,channelName)


class lineObject(object):
    
    def __init__(self, channelName):
        ''''''
                A line input object
        ''''''
        self.channelTypeV = 'LINE'
        inputSourceObject.__init__(self, channelTypeV,channelName)
'''