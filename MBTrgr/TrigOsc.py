import InputSource as inpsrc
import pylab
import visa

# -*- coding: utf-8 -*-
#TRIGGER DEFAULT VALUES
#trigger name
triggerName = 'TRIGGER'
#select trigger source (CH1)
triggerSrcLst = ['AUXiliary', 'CH1', 'CH2', 'CH3', 'CH4', 'LINE']
triggerSrc = triggerSrcLst[1]
triggerCHInt=1
#select trigger slope
triggerSlopeLst = ['RISe','FALL']
triggerSlope = triggerSlopeLst[0]
#select trigger coupling 
triggerCouplingLst = ['DC','AC','HFRej']
triggerCoupling = triggerCouplingLst[0]
#select triggel level (<NR3> - fp with exponent)
triggerLevelLst = ['ECL','TTL']
triggerLevel = triggerLevelLst[1]
#Trigger mode
triggerModeList = ['AUTO','NORMal']
triggerMode = triggerModeList[1]
#Trigger horizontal position
#0-100 (integer)
triggerHPosition = 0
#Trigger termination
triggerTermination = '1E+6'
#Trigger vertical REF position
#triggerVRPosition = 25
#trigger vertical position
triggerVPosition = -1
#vertical scale
triggerVScale = '1E+00'



#TRIGGER SETUP
'''
The constuctor is supposed to be executed if the trigger is connected to channel0 - 4 input
in case of AUXiliary and line inputs, other constructors are to be used
'''
class triggerOscSettingsCH(object):
    
    def __init__(self, setupObject, channelObject, triggerSlope, triggerCoupling, triggerLevel, triggerMode, triggerHPosition):
        
        #scope object
        self.scope = setupObject.scope
        #input the port object
        self.port = channelObject
        #the name for GPIB purpose is automatically retrieved
        print(self.port.noOfChannel)
        self.triggerCHInt = self.port.noOfChannel
        self.changeSlope(triggerSlope)
        self.changeCoupling(triggerCoupling)
        self.changeLevel(triggerLevel)
        self.changeMode(triggerMode)
        self.changeHorizPosition(triggerHPosition)
        #triggerVScale, triggerVPosition, triggerTermination are attributes of inputs source
        print('a change of trigger settings has been performed')


    ''' 
        the working principle of the function is quite complex and may cause some bugs
            TODO: tests
    '''
    def changeEdgeSource(self, channelObject):
        '''
            all the dependent settings have to be reset once again, the dependency is in 
            inputSource class - name, termination, position and scale
        '''
        oldPort = self.port
        self.port = channelObject
        #Set channel of the trigger
        self.scope.write('TRIGger:A:EDGE:SOUrce %s' % channelObject.fullGPIBName)
        #the settings from the last configuration are just moving to new one
        self.port.changeName(oldPort.channelName)
        self.port.changeTermination(oldPort.triggerTermination)
        self.port.changeVerticalPosition(oldPort.triggerVPosition)
        self.port.changeVerticalScale(oldPort.triggerVScale)
        
        


    def changeSlope(self, triggerSlope):
        
        self.triggerSlope = triggerSlope
        #Set edge of the trigger - Slope
        self.scope.write('TRIGger:A:EDGE:SLOpe %s' % triggerSlope)
        

    def changeCoupling(self, triggerCoupling):
        
        self.triggerCoupling = triggerCoupling
        #coupling
        self.scope.write('TRIGger:A:EDGE:COUPling %s' % triggerCoupling)
        

    def changeLevel(self, triggerLevel):
        
        self.triggerLevel = triggerLevel
        #level
        self.scope.write('TRIGger:A:LEVel %s' % triggerLevel)
    
    def changeMode(self, triggerMode):
        
        self.triggerMode = triggerMode
        #mode (normal)
        self.scope.write('TRIGger:A:MODe %s' % triggerMode)
        
    
    def changeHorizPosition(self, triggerHPosition):
        
        self.triggerHPosition = triggerHPosition
        #h-position
        self.scope.write('HORizontal:TRIGger:POSition %d' % triggerHPosition)



'''
The constructors below are to be finished if the input connections will be used in the future
'''
class triggerOscSettingsAUX(object):
    
    def __init__(self, triggerHPosition):
        
        self.triggerHPosition = triggerHPosition

        
class triggerOscSettingsLine(object):
    
    def __init__(self, triggerHPosition):
        
        self.triggerHPosition = triggerHPosition








