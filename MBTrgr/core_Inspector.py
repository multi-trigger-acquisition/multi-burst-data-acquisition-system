#from pyjapc import PyJapc
#import logging
#import sys
import GeneralOsc as gn
import InputSource as isrc
import Setup as sp
import TriggerDevice as trgr
import TrigOsc as tos
import Acquisition as acq
import time
import ReadWriteToFile
import matplotlib.pyplot as plt
import Debugging as db
import ExternalWindow as ew
import sys

#%% Solving the problem with a strange error
#1. go to the ip of the agilent device
#Localize it by : nmap -sP 128.141.96.0/24
#2. change an attribute of the agilent device through the web interface (in a web browser input the ip)


#%%library for memory profiling
#sys.path.insert(0, '/afs/cern.ch/user/p/pakozlow/MultiburstTriggers/MBTrgrPrototype/MBTrgr/libraries')
#from memory_profiler import profile

#%%library for time profiling - doesn't work
#sys.path.insert(0, '/afs/cern.ch/user/p/pakozlow/MultiburstTriggers/MBTrgrPrototype/MBTrgr/libraries')
#from line_profiler import profile

##To use profiling
## @profile

#%% another library for time profiling - works

#import cProfile, pstats
#import io as stringIOModule
#    
#class timeProfile(object):
#    
#    def __init__(self):
#        self.pr = None
#    
#    def start(self):
#        self.pr = cProfile.Profile()
#        self.pr.enable()
#    
#    def finish(self):
#        self.pr.disable()
#        s = stringIOModule.StringIO()
#        sortby = 'cumulative'
#        ps = pstats.Stats(self.pr, stream=s).sort_stats(sortby)
#        ps.print_stats()
#        print(s.getvalue())
#        
#    def startSimpleCnt(self):
#        self.start = time.time()
#        
#    def endSimpleCnt(self):
#        self.end = time.time()
#        overall = self.end - self.start
#        print(overall)
#
#timeprof = timeProfile()
#
##To measure time:
#        #timeprof.start()
#        #functionUnderTest()
#        #timeprof.finish()
#



#%%

#inititalize the functionality of the inspector panel
from inspectorpy import InspectorCom
print('inspectorpy imported')
inspector = InspectorCom()
print('inspector initialized ')




#%% Global variables
noOfTriggers = 16
triggerList = []
acquisitionObj = acq.acquisitionObject(None, None, None, None,None)
generalOscSet = None
listOfVals = gn.returnListOfPossibleValues()
#channel of the beam
channelNo = 0
#existence of the interactive window for plots and analysis
initialised = False

inspector.setVariable('$listOfVals',listOfVals)
print(listOfVals)

'''
value = inspector.getVariable('$nazwa')
while True :
    value = inspector.getVariable('$nazwa')
    print(value)
    time.sleep(2)
'''
#%%
'''
1. Variables and functions necessary for connect action
'''
setupObject = sp.setupObject()
data = []
#inspector.setOutputVariable('$data',data)
#inspector.end()
inspector.setVariable("$activButtons", 0)

inspector.setVariable('$busy', False)
inspector.setVariable('$triggerViewMode', False)

#check if the panel is connected to the scope
inspector.setVariable('$connected', False)
#check if at least the initial settings have been applied
inspector.setVariable('$settingsUpd', False)
inspector.setVariable('$setUser', False)


def connect():
    '''
    Create a global setupObject to use this session later in other methods.
    Connect to the scope with the settings given in input inspector fields.
    Set some attributes related with data transmission - stick to the convention
    in the rest of the program - one sample occupies one byte, the format is 
    just unsigned integer (uint8).
    '''
    global setupObject
    print("connect")
    ip_address = inspector.getVariable('$ip_address')
    gpip_address = inspector.getVariable('$gpib_address')
    #it is not a pyvisa timeout, but rather a custom one
    timeout = inspector.getVariable('$timeout')
    print(ip_address ,' ', gpip_address , ' ' , timeout )
    setupObject.initialize(ip_address, gpip_address, timeout)
    setupObject.rstFactorySettings()
    setupObject.turnOffHeader()
    setupObject.turnOffAcqSystem()
    inspector.setVariable('$connected', True)
    setupObject.setDataFormat()
    #create a Debugging object
    global debuggingTools
    debuggingTools = db.Debugging(setupObject)
    
  


#%%

ch1_trigger = isrc.channelObject('trigger',setupObject)
ch2_beam = isrc.channelObject('beam',setupObject)


def updateScopeSettings():
    '''
    2. Choose ports and specific settings for the beam and trigger
    connect beam (ch2)
    connect trigger (ch1)
    '''
    #
    triggerChannel = inspector.getVariable('$triggerChannel')
    triggerVScale = inspector.getVariable('$triggerVScale')
    #
    beamVScale = inspector.getVariable('$beamVScale')
    beamChannel = inspector.getVariable('$beamChannel')
    global channelNo
    channelNo = beamChannel
    beamOffset = float(inspector.getVariable('$beamOffset'))
    
    #
    triggerHPosition = inspector.getVariable('$triggerHPosition')
    horizontalMainPos = inspector.getVariable('$horizontalMainPos')
    #
    HScaleRecLenSamRat = inspector.getVariable('$HScaleRecLenSamRat')
    horizontalPosition = inspector.getVariable('$horizontalPosition')
    fastFrameReferenceFrame = inspector.getVariable('$fastFrameReferenceFrame')
    fastFrameFrameCount = inspector.getVariable('$fastFrameFrameCount')
    print('fastFrameFrameCount ',fastFrameFrameCount)
    triggerVPosition = inspector.getVariable('$triggerVPosition')
    beamVPosition = inspector.getVariable('$beamVPosition')
    
    HScaleRecLenSamRat_d = HScaleRecLenSamRat.split(",")
    horizontalMainScale = float(HScaleRecLenSamRat_d[0])
    print("HMS",horizontalMainScale)
    mainSampleRate = float(HScaleRecLenSamRat_d[1])
    print("MSR",mainSampleRate)
    horizontalResolution = float(HScaleRecLenSamRat_d[2])
    print("HR",horizontalResolution)
    horizontalRecordLen = horizontalResolution
    fastFrameFrameLength = horizontalResolution
    
    couplingLst = ['DC','AC']
    
    #Trigger channel initialisation
    #triggerChannel = 1
    #trigger vertical position
    #triggerVPosition = -2
    #trigger termination
    triggerTermination = '1E+6'
    #vertical scale (2V)
    #triggerVScale = '14E-1'
    #select trigger coupling
    triggerCoupling = couplingLst[0]
    
    #ch1_trigger = isrc.channelObject('trigger',triggerChannel, setupObject, triggerTermination, triggerVPosition, triggerVScale, triggerCoupling) 
    ch1_trigger.initialisation(triggerChannel, setupObject, triggerTermination, triggerVPosition, triggerVScale, triggerCoupling,0)
        
    
    #%%
    #Beam channel initialisation
    # Select data source
    #beamChannel = 2
    # select termination
    beamTermination = '1E+6'
    #beam v position
    #beamVPosition = 1
    #vertical scale [e.g. 100mv per division]
    #beamVScale = '5E-01'
    #beam coupling
    beamCoupling = couplingLst[1]
    global ch2_beam
    ch2_beam.initialisation(beamChannel, setupObject, beamTermination, beamVPosition, beamVScale, beamCoupling,beamOffset)
    ch2_beam.turnOn()
    #%%
    '''
    set trigger settings on the oscilloscope
    '''
    #select trigger slope
    triggerSlopeLst = ['RISe','FALL']
    triggerSlope = triggerSlopeLst[0]
    #select trigger coupling 
    triggerCouplingLst = ['DC','AC','HFRej']
    triggerCoupling = triggerCouplingLst[0]
    #select triggel level (<NR3> - fp with exponent)
    triggerLevelLst = ['ECL','TTL']
    triggerLevel = triggerLevelLst[1]
    #Trigger mode
    triggerModeList = ['AUTO','NORMal']
    triggerMode = triggerModeList[1]
    #Trigger horizontal position
    #0-100 (integer)
    #triggerHPosition = 24
    #in the method the coupling can have different values from AC & DC, also HFRej, LFRej and NOISErej
    triggerOscSetCHobj = tos.triggerOscSettingsCH(setupObject,ch1_trigger,triggerSlope,triggerCoupling,triggerLevel,triggerMode,triggerHPosition)
    triggerViewMode = inspector.getVariable('$triggerViewMode')

        
    #%%
    '''
    setting the general settings
    '''
    #GENERAL VIEW
    ##Main time base
    #Horizontal scale (2 us per divison)
    #horizontalMainScale = '2.0E-7' 
    #horizontal main position
    #it has an influence on trigger position!
    #horizontalMainPos = '6E+1'#50
    #sample rate, attention! has influence on record length
    #1.25GS/s
    
    #mainSampleRate = '2.5E9'
    ##position on the display
    #horizontal scale is equivalent to horizontal main scale
    #horizontalScale
    #horizontal resolution e.g. 5000 = 5000 data points will be acquired each record
    #horizontalResolution = 5000
    #horizontal position -10% ov waveform is to the left of the screen center
    #horizontalPosition = 50
    #horizontal recordlength -sets record length to the number of data points in each frame 
    #atention! has influence on sample rate
    #horizontalRecordLen =  5000
    ##FastFrame - memory segmentation, captures a series of triggered acquisitions with minimal intervening time.
    #reference frame - 2nd frame
    #fastFrameReferenceFrame = 1
    #frame count - number of frames to acquire
    #fastFrameFrameCount = 2
    #frame length
    #fastFrameFrameLength = 5000
    global generalOscSet
    generalOscSet = gn.generalOscSetObject(setupObject,horizontalMainScale, horizontalMainPos, mainSampleRate, horizontalResolution, horizontalPosition, horizontalRecordLen, fastFrameReferenceFrame, fastFrameFrameCount, fastFrameFrameLength)
    #prepare an acquisition object on the base of the settings
    global acquisitionObj
    acquisitionObj = acq.acquisitionObject(setupObject, "single acquisition", channelNo, generalOscSet, ch2_beam)
    
    if(triggerViewMode==True):
        ch1_trigger.turnOn()
    else:
        ch1_trigger.turnOff()

#%%
#cycleName =  ''  
#triggerList = 1
def readAllTriggers(triggerList):
    #arrays of values
    for x in range(1, noOfTriggers+1):
        #print(x)
        #print(triggerList)
        #enable attribute stored directly in device
        trig1 = triggerList[x-1]
        enabled = trig1.triggerEnabled
        #enable attribute viewed by inspector panel
        inspector.setVariable('$S%dMEnable'%(x),enabled)
        turnDistanceDev = triggerList[x-1].turnDistance
        #print(turnDistanceDev)
        inspector.setVariable('$S%dMTurnDist'%(x), turnDistanceDev)
        CTimeDev = triggerList[x-1].cTime
        #print(CTimeDev)
        inspector.setVariable('$S%dMCTime'%(x), CTimeDev)
        NoOfFramesDev = triggerList[x-1].numberFrames
        inspector.setVariable('$S%dMNoFrames'%(x), NoOfFramesDev)
        CtimeDelayDev = triggerList[x-1].cTimeDelay
        inspector.setVariable('$S%dMCtimeDelay'%(x),CtimeDelayDev)
    
'''
Generation of string of values for inspector
attributes=''
for x in range(1, 9):
    attributes = attributes + ',' + ('$S%dMEnable'%(x))
    attributes = attributes + ',' + ('$S%dMTurnDist'%(x))
    attributes = attributes + ',' + ('$S%dMCTime'%(x))
    attributes = attributes + ',' + ('$S%dMNoFrames'%(x))
    attributes = attributes + ',' + ('$S%dMCtimeDelay'%(x))
attributes
'''           

#%%
def setUser():
    '''
    1. Establish connection to triggering device
    '''
    global triggerList
    cycleName = inspector.getVariable('$cycleName')
    #cycleName = 'CPS.USER.LHC2'
    triggerList = trgr.establishConnectivity(cycleName)
    #trigg = triggerList[0]
    #print(triggerList)
    #trigg.cTimeDelay
    readAllTriggers(triggerList)
    inspector.setVariable('$setUser', True)
   
#%%
def cleanTriggers():
    '''
    2. Clear all the other users 
    '''
    global triggerList
    trgr.disableAllTriggers()
    #print('all trigers disabled')
    for x in range(1, noOfTriggers+1):
        trig1 = triggerList[x-1]
        enabled = trig1.triggerEnabled
        inspector.setVariable('$S%dMEnable'%(x),enabled)    
    
#%%
def updateTriggers():
    global triggerList
    #print("size", len(triggerList))
    #check the previous values
    #apply change if necessary
    # calcNoOfFrames - a variable which automatically calculates the number of frames and assign it to the scope
    calcNoOfFrames = 0
    for x in range(1, noOfTriggers+1):
        #add frames only if a given trigger is enabled
        addFrames = False
        enable = inspector.getVariable('$S%dMEnable'%(x))
        
        if(enable == True):
            addFrames = True
        
        enableDev = triggerList[x-1].triggerEnabled
        if(enable != enableDev ):
            print("changed")
            if(enable == True):
                triggerList[x-1].enableTrigger()
            else :
                triggerList[x-1].disableTrigger()
        
        turnDistance = inspector.getVariable('$S%dMTurnDist'%(x))
        turnDistanceDev = triggerList[x-1].turnDistance
        #print("turnDistance set in inspector: " + str(turnDistance))
        #print("turnDistance set in the device: " + str(turnDistanceDev)) 
        if(turnDistance != turnDistanceDev ):
            #print("changed")
            triggerList[x-1].changeTurnDistance(turnDistance)
        
        CTime = inspector.getVariable('$S%dMCTime'%(x))
        CTimeDev = triggerList[x-1].cTime
        if(CTime != CTimeDev ):
            triggerList[x-1].changeCTime(CTime)
        
        NoOfFrames = inspector.getVariable('$S%dMNoFrames'%(x))
        #if enabled then sum the frames up from all the enabled triggers
        if(addFrames == True):
            calcNoOfFrames=calcNoOfFrames+NoOfFrames
            
        NoOfFramesDev = triggerList[x-1].numberFrames
        if(NoOfFrames != NoOfFramesDev ):
            triggerList[x-1].changeNumberFrames(NoOfFrames)
        
        CtimeDelay = inspector.getVariable('$S%dMCtimeDelay'%(x))
        CtimeDelayDev = triggerList[x-1].cTimeDelay
        if(CtimeDelay != CtimeDelayDev ):
            triggerList[x-1].changeCTimeDelay(CtimeDelay)
    inspector.setVariable('$fastFrameFrameCount', calcNoOfFrames)
    
    cycleName = inspector.getVariable('$cycleName')
    if(cycleName=='CPS.USER.LHC1' or cycleName=='CPS.USER.LHC2' or cycleName=='CPS.USER.LHC3' or cycleName=='CPS.USER.LHC4'):
        acquisitionObj.plotProgramWithTriggers(inspector)
#%% 
def recalibrate():
    setupObject.calibrate()
    print('calibrated')
#%%
def wrSettings():
    ReadWriteToFile.writeSettingsToFile(inspector)
#%%    
def rdSettings():
    ReadWriteToFile.readSettingsFromFile(inspector)
#%%     
def acqData():
    ch1_trigger.turnOff()
    
    #%%library for memory profiling
    sys.path.insert(0, '/afs/cern.ch/user/p/pakozlow/MultiburstTriggers/MBTrgrPrototype/MBTrgr/libraries')
    import testing_connectivity
    

    
    
    print("running acquisition")
    print("FF count " , generalOscSet.fastFrameFrameCount)
    global acquiredData
    acquiredData, acqSuccess = acquisitionObj.acquireData(setupObject.SWtimeout/1000)
    print("timeout set :" , setupObject.SWtimeout)
    inspector.setVariable('$acqSuccess', acqSuccess)
    
    #Test transmission only on the ZERO cycle
    testTrans=False
    #test of transmission time
    if(testTrans==True):
        test = testing_connectivity.test
        test.test1(setupObject.scope, debuggingTools,setupObject)
    
    
    
#%%
    '''
    This function is supposed to pop up a new window for the advanced plots and data analysis.
    It also creates a handle for both plots - gradient and one-turn profile
   '''   
def makeAFigure():
    #create a global handle for the external window
    global popUpWindow
    popUpWindow=ew.ExternalWindow()
    #set the atribute to give the information that the window for plots and analysis already exist
    initialised = True
    
#%% 
'''
Advanced data plot. It is a more dynamic version, it runs in new window and support animation.
'''
def AdvancedPlotData(popUpWindow):

        
    
    #which mode of scalling - consistent with divisions or 
    modePosOff = inspector.getVariable('$modePosOff')
    
    #plot one turn in new window (expert mode)
    #select frame to beam one-turn plot
    NoOfFrame = inspector.getVariable('$NoOfFrame')
    
    #Check if the frame exist
    fastFrameFrameCount = setupObject.scope.ask('HORizontal:FASTframe:COUNt?')
    if(int(NoOfFrame)>int(fastFrameFrameCount)):
        print("the chosen frame doesnt exist, returned to 1")
        NoOfFrame=1
        inspector.setVariable('$NoOfFrame', 1)
    
    #modify ax - use the handle to modify content in pop up window
    
    #plot one turn in new window (expert mode)
    acquisitionObj.AdvancedPlotFrame(modePosOff,NoOfFrame,popUpWindow.frameFigure)
    
    #plot gradient in new window (expert mode)
    acquisitionObj.AdvancedPlotGradient(modePosOff, popUpWindow.gradientFigure)

#%%
    '''
    Plot basic data plots -optimized for the time of plotting but without detailed information.
    They are supposed to be embedded in the panel and used only for adjustment of settings for
    future data acquisition.
    '''
def plotData():
    
    
    #which mode of scalling - consistent with divisions or 
    modePosOff = inspector.getVariable('$modePosOff')
    #modify ax

    #plot one turn in new window (expert mode)
    #select frame to beam one-turn plot
    NoOfFrame = inspector.getVariable('$NoOfFrame')
    
    #Check if the frame exist
    fastFrameFrameCount = setupObject.scope.ask('HORizontal:FASTframe:COUNt?')[:-1]
    print("fastFrameFrameCount",fastFrameFrameCount)
    print("NoOfFrame",NoOfFrame)
    if(int(NoOfFrame)>int(fastFrameFrameCount)):
        print("the chosen frame doesnt exist, returned to 1")
        NoOfFrame=1
        inspector.setVariable('$NoOfFrame', 1)
    
    #plot one turn profile in the same window (simple mode)
    acquisitionObj.plotFrame(modePosOff,NoOfFrame)
    
    #plot gradient in the same window (simple mode)
    acquisitionObj.plotGradient(modePosOff)

    

#%%
def saveDataToFile():
    name = inspector.getVariable('$name')
    modePosOff = inspector.getVariable('$modePosOff')
    modeSc = inspector.getVariable('$modeSc')
    acquisitionObj.saveToFile(name,modePosOff,modeSc)
    inspector.setVariable('$Console', 'The data has been stored')

#%%
def addNewConfOfScalling():
    #HorizontalScale = inspector.getVariable('$HorizontalScale')
    SampleRate = inspector.getVariable('$SampleRate')
    HorizontalResolution = inspector.getVariable('$HorizontalResolution')
    #take number of points and number of frames
    HorizontalScale = (HorizontalResolution/4E10)/10
    #verify if it meets the requirements for max number of samples
    inspector.setVariable('$HorizontalScale', HorizontalScale)
    noOfFrames = inspector.getVariable('$fastFrameFrameCount')
    maxframes = int(setupObject.scope.ask('HORizontal:FASTframe:MAXFRames?'))
    if(maxframes>noOfFrames):   
        Console = 'The max number of frames for this configuration is %d' %maxframes
        inspector.setVariable('$Console', Console)
    else:
        Console = 'The max number of frames :%d . ERROR: decrease number of frames' %maxframes
        inspector.setVariable('$Console', Console)
    
    HScaleRecLenSamRat_ = "%e" %HorizontalScale +"," + "%e" %SampleRate+ "," + "%e" %HorizontalResolution
    inspector.setVariable('$HScaleRecLenSamRat', HScaleRecLenSamRat_)


#%%
'''
The following loop runs in background and works as an event handler. It is an approach to make the interactive reaction of 
the python script independent from inspector logics, but uses inspector input options. It might affect the performance of the
application, but if so the influence is negligible but the code is more clear than spliting the logics into many python scripts
and attaching to separate blueprints. In the other approach it could happen that a lot of logics would be duplicated - blueprints
do not share the same context.
'''
while True:
    activButtons = inspector.getVariable('$activButtons')
    #loop over array of buttons
    if activButtons == 0: # noOP state
        time.sleep(1)
        #print('no action')
    elif activButtons == 1: # connect
        connect()
        print('the connection has been established')
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)
    elif activButtons == 2: # update scope settings
        updateScopeSettings()
        print('the scope settings have been changed')
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 3: # refresh the scope screen on the panel
        print("refresh the scope screen on the panel")        
        debuggingTools.takeScreenshot(setupObject)
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)
    elif activButtons == 4: # recallibrate
        print("recallibrate")
        recalibrate()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)
    elif activButtons == 5: # clean triggers
        cleanTriggers()
        print('clean triggers ')
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)  
    elif activButtons == 6: # update triggers
        updateTriggers()
        print('update trigger')
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 7: # set user
        setUser()
        print('set user ')
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 8: # advanced plot
        print("advanced plot")
        if(initialised!=True):
            makeAFigure()
            initialised=True
        AdvancedPlotData(popUpWindow)
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)
    elif activButtons == 9: # read settings
        print("read settings")
        rdSettings()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 10: # write settings
        print("write settings")
        wrSettings()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 11: # acquire data
        print("acquire data")
        acqData()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 12: # plot
        print("plot")
        plotData()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False) 
    elif activButtons == 13: # save to file
        print("save to file")
        saveDataToFile()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)
    elif activButtons == 14: # add new settings for HorizontalScale, SampleRate, HorizontalResolution
        print("add new settings for HorizontalScale, SampleRate, HorizontalResolution")
        addNewConfOfScalling()
        inspector.setVariable("$activButtons", 0)
        inspector.setVariable('$busy', False)  
    
inspector.end()