from inspectorpy import InspectorCom
import json


#
#print('inspectorpy imported')
#inspector = InspectorCom()
#print('inspector initialized ')

noOfTriggers = 16

class PanelWrite:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
                         sort_keys=True, indent=4)
        
#write
def writeSettingsToFile(inspector):
    #name of the file
       
    name = inspector.getVariable('$name')
    #print(name)
    panel = PanelWrite()
    #scope
    panel.ip_address = inspector.getVariable('$ip_address')
    panel.gpib_address = inspector.getVariable('$gpib_address') 
    panel.timeout = inspector.getVariable('$timeout')
    panel.triggerChannel = inspector.getVariable('$triggerChannel') 
    panel.beamChannel = inspector.getVariable('$beamChannel')    
    panel.triggerVScale = inspector.getVariable('$triggerVScale')    
    panel.beamVScale = inspector.getVariable('$beamVScale') 
    panel.triggerHPosition = inspector.getVariable('$triggerHPosition')
    panel.horizontalMainPos = inspector.getVariable('$horizontalMainPos')    
    panel.horizontalPosition = inspector.getVariable('$horizontalPosition')
    panel.fastFrameReferenceFrame = inspector.getVariable('$fastFrameReferenceFrame')
    panel.fastFrameFrameCount = inspector.getVariable('$fastFrameFrameCount')
    panel.HScaleRecLenSamRat = inspector.getVariable('$HScaleRecLenSamRat')    
    panel.triggerVPosition = inspector.getVariable('$triggerVPosition')   
    panel.beamVPosition = inspector.getVariable('$beamVPosition')
    panel.beamOffset = inspector.getVariable('$beamOffset')
    #trigger    
    panel.cycleName = inspector.getVariable('$cycleName')
    panel.enable = []
    panel.turnDistance = []
    panel.CTime = []
    panel.NoOfFrames = []
    panel.CtimeDelay = []
    for x in range(1, noOfTriggers+1):
        panel.enable.append(inspector.getVariable('$S%dMEnable'%(x)))
        panel.turnDistance.append(inspector.getVariable('$S%dMTurnDist'%(x)))
        panel.CTime.append(inspector.getVariable('$S%dMCTime'%(x)))
        panel.NoOfFrames.append(inspector.getVariable('$S%dMNoFrames'%(x)))
        panel.CtimeDelay.append(inspector.getVariable('$S%dMCtimeDelay'%(x)))
        
    with open('savedSettings/%s.json' %name, 'w') as outfile:
        print(panel.toJSON(), file=outfile)
    #print(panel.toJSON())


class PanelRead:
    def __init__(self, j):
        self.__dict__ = json.loads(j)

    #read
def readSettingsFromFile(inspector):
    #name of the file
    name = inspector.getVariable('$name')
    #print(name)
    with open('savedSettings/%s.json' %name) as json_file:
        parsed = json.load(json_file)
        stringData = json.dumps(parsed, indent=4, sort_keys=True)
        #print(stringData)
        panel = PanelRead(stringData)
         
    inspector.setVariable('$ip_address', panel.ip_address)
    inspector.setVariable('$gpib_address', panel.gpib_address)
    inspector.setVariable('$timeout', panel.timeout)
    inspector.setVariable('$triggerChannel', panel.triggerChannel)
    inspector.setVariable('$beamChannel', panel.beamChannel)
    inspector.setVariable('$triggerVScale', panel.triggerVScale)
    inspector.setVariable('$beamVScale', panel.beamVScale)   
    inspector.setVariable('$triggerHPosition', panel.triggerHPosition)
    inspector.setVariable('$horizontalMainPos', panel.horizontalMainPos)
    inspector.setVariable('$horizontalPosition', panel.horizontalPosition)
    inspector.setVariable('$fastFrameReferenceFrame', panel.fastFrameReferenceFrame)
    inspector.setVariable('$fastFrameFrameCount', panel.fastFrameFrameCount)
    inspector.setVariable('$cycleName', panel.cycleName)
    inspector.setVariable('$HScaleRecLenSamRat', panel.HScaleRecLenSamRat)
    inspector.setVariable('$triggerVPosition', panel.triggerVPosition)
    inspector.setVariable('$beamVPosition', panel.beamVPosition)
    inspector.setVariable('$beamOffset', panel.beamOffset)
    
    for x in range(1, noOfTriggers+1):
        enableDev = panel.enable[x-1]
        inspector.setVariable('$S%dMEnable'%(x),enableDev)
        
        turnDistanceDev = panel.turnDistance[x-1]
        inspector.setVariable('$S%dMTurnDist'%(x), turnDistanceDev)
        
        CTimeDev = panel.CTime[x-1]
        inspector.setVariable('$S%dMCTime'%(x), CTimeDev)
        
        NoOfFramesDev = panel.NoOfFrames[x-1]
        inspector.setVariable('$S%dMNoFrames'%(x), NoOfFramesDev)
        
        CtimeDelayDev = panel.CtimeDelay[x-1]
        inspector.setVariable('$S%dMCtimeDelay'%(x), CtimeDelayDev)




#inspector.end()
