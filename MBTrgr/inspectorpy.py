#############################################################################
## Inspector - Python Bridge
## Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
## Author: Valter Costa <valter.costa@cern.ch>
## Version: 12
#############################################################################

  
import sys
import base64
import struct
import socket
import ast
import binascii 
from threading import Thread
from threading import Lock



  #####################
  ## Synchronization functions
  #####################


def synchronized_class_method(func):
  def decorator(self, *args, **kwargs):
    with self.__lock__:
     return func(self, *args, **kwargs)
  return decorator


def synchronized_class(sync_class):
  
  lock = Lock()
  init = sync_class.__init__
    
  def __init__(self, *args, **kws):
    self.__lock__ = lock
    init(self, *args, **kws)
        
  sync_class.__init__ = __init__
  return sync_class


#############################################################################


  #####################
  ## Inspector Bridge class
  #####################


@synchronized_class
class InspectorCom:
  
  
  def __init__(self):
    self.raw_data = (sys.stdin.buffer if sys.version_info[0] >= 3 else sys.stdin).read()
    self.output_data = {}
    self.__processInput()
    
    self.inspectorPort = self.getInputVariable('_system:socket_port')
    self.currentLibraryVersion = self.getInputVariable('_system:current_lib_version')
    self.network = _InspectorNetworkManager(self.inspectorPort);

    self.caster = {'ENUM': str,
                  'FLOAT': float,
                  'DOUBLE': float,
                  'INTEGER': int,
                  'SHORT': int,
                  'BYTE': int,
                  'LONG': int,
                  'BOOLEAN': self.__convertToBool,
                  'STRING': str,
                  'ARRAY': self.__literalEval,
                  'STATUSENUM': str,
                  'NONE': str,
                  'DISCRETE_FUNCTION': self.__literalEval,
                  'DISCRETE_FUNCTION_LIST':  self.__literalEval
                  }
                  
                  
  #####################
  ## Private methods
  #####################


  def __processInput(self):
        
    if len(self.raw_data) <= 11:
      return
        
    inspector_check = self.raw_data[:11].decode()
    
    if inspector_check != '^~Inspector':
      # Probably no data was transmitted
      return
    
    total_data_size = int(struct.unpack(">I", self.raw_data[11:15])[0]) + 1
    
    if len(self.raw_data) < 15 + total_data_size:
      size_to_read = (15 + total_data_size) - len(self.raw_data)
      self.raw_data = self.raw_data + (sys.stdin.buffer if sys.version_info[0] >= 3 else sys.stdin).read(size_to_read)
    
    index = 15
    self.data = {}

    while index < total_data_size:
      data_size = int(struct.unpack(">I", self.raw_data[index:index + 4])[0])
      data = self.raw_data[index + 4:index + 4 + data_size]
      data_name, data_value = data.decode().split('\0')
      self.data[base64.b64decode(data_name).decode()] = base64.b64decode(data_value).decode()
      index += 4 + data_size 
      

  def __processAndCommitOutput(self):
    
    if sys.version_info[0] >= 3:
      
      raw_output = '^~Inspector'.encode()
      data_size = 0
      raw_data = bytearray()
      
      for dkey, dvalue in self.output_data.items():
        name = base64.b64encode(str(dkey).encode())
        value = base64.b64encode(str(dvalue).encode())
        size = len(name) + len(value) + 1
        data_size += 4 + size
        raw_data += struct.pack("!I", size) + (name + '\0'.encode() + value)
         
      raw_output += struct.pack("!I", data_size)
      raw_output += raw_data
          
    
      sys.stdout.buffer.write(raw_output)
      
    else:
      
      raw_output = '^~Inspector'
      data_size = 0
      raw_data = ''
      
      for dkey, dvalue in self.output_data.items():
        name = base64.b64encode(str(dkey))
        value = base64.b64encode(str(dvalue))
        size = len(name) + len(value) + 1
        data_size += 4 + size
        raw_data += struct.pack("!I", size) + (name + '\0' + value)
         
      raw_output += struct.pack("!I", data_size)
      raw_output += raw_data
      
      sys.stdout.write(''.join(raw_output))


  #####################
  ## Conversions and Casting
  #####################


  def __convertToNumber(self, value):
    try:
      return int(value)
    except ValueError:
      return float(value)


  def __convertToBool(self, value):
    return {'true': True, 'false': False}[value.lower()]


  def __literalEval(self, value):
    var = str(value)     
    return ast.literal_eval(var)


  def _typeCast(self, value, typeName):
    return self.caster.get(typeName, self.__autoCast)(value)
    
    
  def __autoCast(self, value):
    strValue = str(value)
    
    for f in (self.__convertToBool, int, float, self.__literalEval):
        try:
            return f(strValue)
        except (ValueError, KeyError, SyntaxError):
            pass
            
    return value 


  #####################
  ## Variable getters  
  #####################
  
  
  def getInputVariable(self, name):
    return self.__autoCast(self.data[name])
                
        
  #####################
  ## Variable setter   
  #####################
    
    
  def setOutputVariable(self, name, value):
    self.output_data[name] = value
    
    
  #####################
  ## Generic public methods
  #####################
    
    
  def getLibVersion(self):
    return 12
    
    
  def getLatestLibVersion(self):
    return self.currentLibraryVersion
    
    
  def end(self):
    self.network.shutdown()
    self.__processAndCommitOutput()
    
    
  #####################
  ## Networking part
  #####################
  

  @synchronized_class_method
  def readJAPC(self, uri):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.READ, _InspectorMessageData("URI", str(uri)))
    reply = self.network.sendMessageAndWaitForReply(message)
    
    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to read the property")
    else:
      typeName = next(d.value for d in reply.data if d.name == "Type")
      return InspectorValue(next(self._typeCast(d.value, typeName) for d in reply.data if d.name == "Value"), next(float(d.value) for d in reply.data if d.name == "Timestamp"), typeName)
   
  
  @synchronized_class_method
  def readNextJAPC(self, uri):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.READ_NEXT, _InspectorMessageData("URI", str(uri)))
    reply = self.network.sendMessageAndWaitForReply(message)
    
    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to read the property")
    else:
      typeName = next(d.value for d in reply.data if d.name == "Type")
      return InspectorValue(next(self._typeCast(d.value, typeName) for d in reply.data if d.name == "Value"), next(float(d.value) for d in reply.data if d.name == "Timestamp"), typeName)
    
  
  @synchronized_class_method 
  def writeJAPC(self, uri, value):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.WRITE, _InspectorMessageData("URI", str(uri)), _InspectorMessageData("Value", str(value)))    
    reply = self.network.sendMessageAndWaitForReply(message)
    
    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to write the property")
   
   
  @synchronized_class_method
  def getVariable(self, name):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.GET_VARIABLE, _InspectorMessageData("Name", str(name)))    
    reply = self.network.sendMessageAndWaitForReply(message)
    
    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to get variable")
    else:
      return next(self.__autoCast(d.value) for d in reply.data if d.name == "Value")
      
  
  @synchronized_class_method 
  def setVariable(self, name, value):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.SET_VARIABLE, _InspectorMessageData("Name", str(name)), _InspectorMessageData("Value", str(value)))    
    reply = self.network.sendMessageAndWaitForReply(message)

    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to set variable")
    
  @synchronized_class_method 
  def setVariableError(self, name, message):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.SET_VARIABLE_ERROR, _InspectorMessageData("Name", str(name)), _InspectorMessageData("Message", str(message)))    
    reply = self.network.sendMessageAndWaitForReply(message)

    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to set variable error")
      
      
  @synchronized_class_method    
  def consolePrint(self, text):
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.CONSOLE_PRINT, _InspectorMessageData("Text", str(text)))    
    reply = self.network.sendMessageAndWaitForReply(message)

    if reply.type == _InspectorNetworkMessage.ERROR:
      raise Exception(reply.data[0].value if len(reply.data) > 0 else "Unable to print to console")
  
  
  def subscribe(self, uri, blocking=True, onValue=None, onException=None):
    return _InspectorJAPCSubscription(self, uri, blocking, onValue, onException)
    
    
#############################################################################
    
    
  #####################
  ## Network manager class
  #####################
    
class _InspectorNetworkManager:
  
  
  def __init__(self, port):
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.socket.connect(("localhost", int(port)))
    
    
  def shutdown(self):
    self.socket.close()
    
    
  def sendMessageAndWaitForReply(self, message):
    self.sendMessage(message)
    return self.receiveMessage()
    
    
  def sendMessage(self, message):
    self.socket.sendall(message.export())
    
    
  def receiveMessage(self):
    dataSize = int(struct.unpack(">I", self.socket.recv(4))[0])
    messageType = int(struct.unpack(">I", self.socket.recv(4))[0])
        
    data = self.receiveAll(dataSize)
    return _InspectorNetworkMessage.build(messageType, data)
    
    
  def receiveAll(self, size):
    
    if sys.version_info[0] >= 3:
      data = bytearray()
    else:
      data = ''
    
    while size > 0:
      newData = self.socket.recv(size)       
      data += newData
      size -= len(newData)
      
    return data
    
    
#############################################################################   
    
  #####################
  ## Network message class
  #####################

class _InspectorNetworkMessage:
    
  CONNECT = 1 
  DISCONNECT = 2
  ERROR = 3
  DATA = 4
  RESULT = 5
  
  READ = 1001
  WRITE = 1002 
  GET_VARIABLE = 1003 
  SET_VARIABLE = 1004 
  CONSOLE_PRINT = 1005 
  READ_NEXT = 1006
  SUBSCRIBE = 1007
  END_SUBSCRIPTION = 1008
  SET_VARIABLE_ERROR = 1009
  
  @staticmethod
  def build(messageType, rawData):            
    index = 0
    length = len(rawData)
    data = []
    
    while index < length:
      size = int(struct.unpack(">I", rawData[index:index + 4])[0])
      index += 4
      singleData = rawData[index:index + size]
      index += size
      data.append(_InspectorMessageData.build(singleData))
    
    return _InspectorNetworkMessage(messageType, *data)
    
  
  def __init__(self, messageType, *data):        
    self.data = list(data)
    self.type = messageType
    
    
  def __str__(self):
    string = 'InspectorNetworkMessage Data:\n'
    
    for data in self.data:
      string += str(data)
      string += '\n'
      
    string += 'End of InspectorNetworkMessage'
      
    return string
    
    
  def export(self):
      
      rawData = bytearray()
      
      for data in self.data:
        d = data.export();
        rawData += struct.pack(">I", len(d))
        rawData += d
      
      dataLen = struct.pack(">I", len(rawData))
      messageType = struct.pack(">I", self.type)

      return dataLen + messageType + rawData
    
    
#############################################################################
    
  #####################
  ## Message Data class
  #####################
  
class _InspectorMessageData:
  
  
  @staticmethod
  def build(data):          
  
    if sys.version_info[0] >= 3:
      data = data.decode()
      
    split = data.split('\0')
    return _InspectorMessageData(split[0], split[1])
  
  
  def __init__(self, name, value):            
    self.name = str(name)
    self.value = str(value)
    
    
  def __str__(self):
    return "MessageData Name: " + self.name + " - Value: " + self.value
    
    
  def export(self):
    return str(self.name + "\0" + self.value).encode()
    
    
#############################################################################
    
  #####################
  ## Value class
  #####################
  
class InspectorValue:
  
  
  def __init__(self, value, timestamp, valueType):            
    self.value = value
    self.timestamp = timestamp
    self.type = valueType
    
    
  def __str__(self):
    return "InspectorValue Value: " + str(self.value) + " Type: " + str(self.type) + " Timestamp: " + str(self.timestamp)
   
   
#############################################################################
    
  #####################
  ## Subscription class
  #####################
      
class _InspectorJAPCSubscription:
  
  
  def __init__(self, inspector, uri, blocking=True, onValue=None, onException=None):
    
    if onValue == None and onException == None:
      raise Exception("No handler defined for the subscription. You should provide a handler for 'onValue' or 'onException'.")
    
    self._inspector = inspector
    self._networkManager = None
    self.uri = uri;
    self._onValue = onValue
    self._onException = onException
    self._blocking = blocking;
    self._running = True
    
    if blocking:
      self.__init()
    else:
      self.__initAsync()
    

  def __init(self):
    if not self._running:
        return
    
    self._networkManager = _InspectorNetworkManager(self._inspector.inspectorPort)
    
    message = _InspectorNetworkMessage(_InspectorNetworkMessage.SUBSCRIBE, _InspectorMessageData("URI", str(self.uri)))  
    self._networkManager.sendMessage(message)
    
    while self._running:
      try:
        message = self._networkManager.receiveMessage()
      except:
        return
      try:
        self.__handleMessage(message)
      except BaseException as e:
        print(e)
        self.__disposeNetworkManager()
        raise e        
    
    self.__disposeNetworkManager()
    
    
  def __initAsync(self):
    t = Thread(target=lambda: self.__init())
    t.setName('InspectorPython-Subscription-Thread')
    t.setDaemon(True)
    t.start()
  
  
  def start(self):
    if not self._running:
        self._running = True
        
        if self._blocking:
            self.__init()
        else:
            self.__initAsync()
  
  
  def stop(self):
    if self._running:
      self._running = False
      self.__disposeNetworkManager()
  
  
  def __disposeNetworkManager(self):
      networkManager = self._networkManager
      self._networkManager = None
      
      if networkManager is not None:
        try:
          message = _InspectorNetworkMessage(_InspectorNetworkMessage.END_SUBSCRIPTION)  
          networkManager.sendMessage(message)
          networkManager.shutdown()
        except:
          pass
  
  
  def __handleMessage(self, message):
    if message.type == _InspectorNetworkMessage.ERROR:
      raise Exception(message.data[0].value if len(message.data) > 0 else "Unable to set variable")
  
    if message.type == _InspectorNetworkMessage.DATA:
      
      if message.data[0].name == "Exception":  
          self._invokeOnException(message.data[0].value)       
      else:
        typeName = next(d.value for d in message.data if d.name == "Type")
        value = InspectorValue(next(self._inspector._typeCast(d.value, typeName) for d in message.data if d.name == "Value"), next(float(d.value) for d in message.data if d.name == "Timestamp"), typeName)
        
        self._invokeOnValue(value)
        
    else:
      raise Exception(message.data[0].value if len(message.data) > 0 else "Invalid data received!") 
  
  
  def _invokeOnValue(self, value):
    if self._onValue:
      self._onValue(self, value)

    
  def _invokeOnException(self, exception):
    if self._onException:
      self._onException(self, exception)
    
    
#############################################################################
    
#############################################################################
## End Inspector Python Bridge
#############################################################################
