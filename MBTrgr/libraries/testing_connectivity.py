# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pyplot
from datetime import datetime
import os
import visa
import pylab
import time


'''
The set of functions which are dependent on a device/

WARNING!!! - the testing script doesn't include setting the triggering device.
'''
class TDS5104(object):
    
    def __init__(self,scope):
        print("initialized")
        self.scope = scope

    def connect(self):
        # Checking if scope is ok
        print(self.scope.query('*IDN?'))
        
    def acquisition(self, noOfPoints):
        #timeout
        self.scope.timeout = 0
        self.scope.write('HORizontal:FASTframe:COUNt %d' %1)
        self.scope.write('HORizontal.MAIn:SAMPLERate 4E10')
        self.scope.write('HORizontal:RECOrdlength %d' % noOfPoints)
        self.scope.write('HORizontal:RESOlution %d' % noOfPoints)
        
    def transfer(self, noOfPoints):
        self.data = self.scope.query_binary_values('CURV?', datatype='h')
        
class DPO7254(object):
    
    def __init__(self,scope):
        print("initialized")
        self.scope = scope

    def connect(self):
        print(self.scope.query('*IDN?'))
        
    def acquisition(self, noOfPoints,noOfFrames):
        self.noOfPoints = noOfPoints
        #timeout
        self.scope.timeout = None
        self.noOfFrames = noOfFrames
        
        #Calculate a correct scale (period / 10 divisions)
        scale = (self.noOfPoints/4E10)/10
        #scale = (self.noOfPoints/float(4E10))/10
                
        self.scope.write('HORizontal:FASTframe:COUNt %d' %self.noOfFrames)
        self.scope.write('HORizontal:MAIn:SAMPLERate 4E10')
        self.scope.write('HORizontal:RECOrdlength %d' % noOfPoints)
        self.scope.write('HORizontal:MAIn:SCAle %E' %scale)
        #self.scope.write('HORizontal:RESOlution %d' % noOfPoints)
        #self.scope.write('HORizontal:FASTframe:LENgth %d' % noOfPoints)
        
        #check if the settings have been applied correctly
        
           
        self.scope.write('acquire:state 1')
        expectedTime = 100
        start_time = time.time()
        #print(start_time)
        while True:
            elapsed_time = time.time() - start_time
            if(elapsed_time <= expectedTime):
            #exit from the loop if the data is in
                if(int(self.scope.ask("BUSY?")) == 0):
                    print("break")
                    break
                    time.sleep(.2)
                    numberofpoints = int(self.scope.ask('wfmoutpre:nr_pt?'))
                    if(numberofpoints!=noOfPoints*self.noOfFrames): 
                        print("the settings haven't been accepted")
                        print(numberofpoints)
            else:
                self.scope.clear()
                print("break and clear")
                break
        print("acquired")
        
  
        
    def transfer(self):
        start = time.time()
        self.data = np.array((self.scope.query_binary_values('CURV?', datatype='B', is_big_endian=False)), dtype=np.uint8)
        #self.data = data1
        end = time.time()
        if(self.noOfPoints*self.noOfFrames!=len(self.data)):
            print('not correct no of samples : len ', len(self.data))
        timeOfTransfer = end - start
        return timeOfTransfer
            
         
class ADQ214(object):
    
    def __init__(self):
        print("initialized")

    def connect(self):
        print("connect routine")
        

class test():
    
    noOfFrames=0
    
    def __init__(self):
        print("initialized")
    
    def test1(scope,debuggingTools,setupObject):
        '''
        Running the tests
        '''
        
        
        
        noOfFrames = 1
        #noOfSamples = np.array([500, 2000, 5000, 10000, 20000, 100000, 500000, 1000000, 5000000, 10000000], np.int32)
        noOfSamples = np.array([1000, 4000, 10000, 40000, 100000], np.int32)
        device = DPO7254(scope)
        device.connect()
        #Repeat experiment 3 times, load the results to separate arrays
        '''
        There are many attempts to measure one scenario in order to establish the deviation
        '''
        #results = np.array([[2.12, 5.6, 1.99], [12.33, 10.23, 14.22], [40.12,47.22,44.1]], np.float32)
        noOfTests = 5
        noOfCases = len(noOfSamples)
        
        
        results = np.empty(shape=[noOfCases, noOfTests])
        for attempt in range(0, noOfTests):
            for x in range(0, noOfCases):
                #test with 100 frames
                device.acquisition(noOfSamples[x],noOfFrames)
                timeTrans = device.transfer()
                results[x,attempt] = timeTrans
                print(timeTrans)
                debuggingTools.takeScreenshot(setupObject)
        
        #print
        
        #print(device.data(1))
        
        
        
        '''
        Visualising the results
        '''
        
        
        
        mean_results = np.mean(results, axis=1)
        print(mean_results)
        min_results = np.min(results, axis=1)
        print(min_results)
        max_results = np.max(results, axis=1)
        print(max_results)
        
        def r_squared(actual, ideal):
            actual_mean = np.mean(actual)
            ideal_dev = np.sum([(val - actual_mean)**2 for val in ideal])
            actual_dev = np.sum([(val - actual_mean)**2 for val in actual])
        
            return ideal_dev / actual_dev
        
        def temp_plot(noOfSamples, mean_results, min_results = None, max_results = None):
        
            #year_start = datetime(2012, 1, 1)
            #days = np.array([(d - year_start).days + 1 for d in noOfSamples])
            fig = pyplot.figure()
            pyplot.title('Measurement of the time of transmission for %d frames'% (noOfFrames) )
            pyplot.ylabel('Mean time')
            pyplot.xlabel('The number of samples per one frame')
        
            if (max_results is None or min_results is None):
                # Normal plot without error bars
                pyplot.plot(noOfSamples, mean_results, marker='o')
            else:
                # Compute min/max temperature difference from the mean
                temp_err = np.row_stack((mean_results - min_results,
                                         max_results - mean_results))
        
                # Make line plot with error bars to show temperature range
                pyplot.errorbar(noOfSamples, mean_results, marker='o', yerr=temp_err)
                pyplot.title('Time of transmission (max/min)')
        
            slope, intercept = np.polyfit(noOfSamples, mean_results, 1)
            ideal_temps = intercept + (slope * noOfSamples)
            r_sq = r_squared(mean_results, ideal_temps)
        
            fit_label = 'Linear fit ({0:.2f})'.format(slope)
            pyplot.plot(noOfSamples, ideal_temps, color='red', linestyle='--', label=fit_label)
            pyplot.annotate('r^2 = {0:2f}'.format(r_sq), (0.05, 0.9), xycoords='axes fraction')
            pyplot.legend(loc='lower right')
        
            return fig
        
        #--------------------------------------------------
        data = [noOfSamples, mean_results, min_results, max_results]
        
        
        if not os.path.exists('plots'):
            os.mkdir('plots')
        
        # Plot without error bars
        #fig = temp_plot(noOfSamples, mean_results)
        #fig.savefig('plots/day_vs_temp.png')
        
        # Plot with error bars
        fig = temp_plot(noOfSamples, mean_results, min_results, max_results)
        fig.savefig('plots/day_vs_temp-all.png')