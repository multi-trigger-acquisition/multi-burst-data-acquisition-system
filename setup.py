'''
Setup file
Adjusts the Inspector panel paths
'''

import os

if __name__ == '__main__':

    project_path = os.path.dirname(os.path.realpath(__file__))

    os.system('rm %s/TriggerInspector/GUIMBrstTrgr.xml' % (project_path))

    GUIFileModel = open('%s/TriggerInspector/GUIMBrstTrgrTemplate.xml'
                        % (project_path))
    GUIFileNew = open('%s/TriggerInspector/GUIMBrstTrgr.xml'
                      % (project_path), 'w')

    for line in GUIFileModel:
        line = line.replace('PROJECTPATH', project_path)
        GUIFileNew.write(line)

    GUIFileModel.close()
    GUIFileNew.close()

    print('Project path adjusted to %s' % (project_path))
